package gholdem;

import static gholdem.protocolEnums.DIV.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class ConfigHandler
{
    static String targetIP;
    static int targetPort;
    
    //pretty straight forward: loads configuration if the configuration file is present, if not target IP/port is set to default values.
    //return value indicates whether configuration file existed or not.
    public static boolean loadConfigurationFromFile()
    {
        //What this function does is to simply load saved info,
        File configFile = new File(CONFIGFILEPATH);
        
        if ( configFile.isFile() )//it exists, we proceed to load it
        {
            try (BufferedReader reader = Files.newBufferedReader(configFile.toPath() ))
            {
                //first line is the IP, second is the port
                targetIP = reader.readLine();
                targetPort = Integer.parseInt(reader.readLine() );
                return true;
            }
            catch (IOException ex)
            {
                System.out.println("IOException occured: "+ex.getMessage());
            }
        }
        else//file doesn't exist, set targetIP and targetPort with default values, IE localhost at 3500
        {
            targetIP = "127.0.0.1";
            targetPort = DEFAULTPORT;
        }
        return false;
    }
    
    //creates or overwrites the configuration file with recently supplied IP and port.
    //Also saves the new data to the targetIP/port variables which may be accessed later from elsewhere.
    public static void writeConfigToFile(String newIP,int newPort)
    {
        targetIP = newIP;
        targetPort = newPort;
        
        //Simple, open the configuration file
        File configFile = new File(CONFIGFILEPATH);        
        
        try (BufferedWriter Writer = Files.newBufferedWriter(configFile.toPath(), Charset.defaultCharset()))
        {
            //overwrite existing data.
            Writer.write(newIP);
            Writer.newLine();
            Writer.write(""+newPort);
            Writer.newLine();
        }
        catch (IOException ex)
        {
            System.out.println("IOException occured: " + ex.getMessage());
        }
    }
    
    public static String getIPTarget() { return targetIP; }
    
    public static int getPortTarget() { return targetPort; }
}