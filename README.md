# Welcome to Gjerstads Texas Holdem: Java Client.

## A port

I have made a texas holdem game made in C++ named Gjerstads Texas Holdem(no points for originality, I know). As of this writing the project exists solely in C++. I aim to port the client portion
to Java.

While either portion of the software is lightweight by modern hardware standards, the server is the one that has to teoretically scale to serve multiple
users, something the client doesn't have to do. Leaving the server solely as a C++ program makes sense, natively compiled code has less overhead, and Linux
is well established as a server operating system. In practice, it probably makes little difference, even with thousands of users, modern hardware should be able to
handle the load just fine running on just one thread, in theory at any rate. For this reason I have no intention of porting the server part of the program to Java.

When that's said, the main project can be found here: https://bitbucket.org/Petter_G/gjerstads-texasholdem

## Why Java?

I've not written Java in some time, so it's time to freshen up a little. Another reason is that C++ isn't as portable as I'd like. 
While it is indeed possible to make a version that works(IE recompilable without modifications) on both windows and linux, I've relegated such
adaptations to the possible future. Why not some other language you may ask? Well, who knows, maybe I'll port the client to yet another language one day.

## Documentation
In the original project, I did not make many design documents, as the project was and still is managed by one man. This time I will make a simple scetch, a initial class diagram detailing how I plan to deal with the task at hand.
Later on as the development process goes forward, I will make, and keep updated, another class diagram. Indeed a use case diagram will be made at some point.

In addition I will, like in the main project, keep a project plan and a patchnotes file. The patch naming schemes will be simple as before: A datestamp and a description of the changes.

I will make sure to keep any documentation and commentary in english from the start this time around.

## So how do I play? 
First of, you'll need to set up a server in order to play. You can find it over here: https://bitbucket.org/Petter_G/gjerstads-texasholdem/overview. 
After that, you'll need to install Java runtime, if you haven't already. When all that's done, you can download and run the executable. 

Press the source button on the left hand screen, then enter the GHoldem folder, then the dist folder. And finally the file itself(GHoldem.jar). Press it and then press raw. This should
prompt a download of the file itself.

Tiresome? I'd provide a direct link, except updates would probably invalidate the link, meaning it wouldn't point to the newest release.

When that's done, and you got the client running, you may need to configure the client. This can be done via the options menu. There you have to enter two variables: The IP4 address and port number
of the server. If not you'll have to figure out what the IP address of your server actually is.

The easiest way to get up and running is probably to run the client on the same host, no configuration necessary. But it really isn't rocket science.

Tricks to figure out your address? If you're running the server in your local network, get on your server host, open a console window and type in ipconfig if on windows, or
ifconfig if on some Unix distro. You will then get a list of network devices and their IPs, you will need to try them out until you get lucky. Chances are the one you're looking for begins
with 192.168. As a note, this is the private IP address of your server's host, it cannot be used to connect from outside your local network.

So what if you're not hosting the server locally? Drop by this site: https://www.whatismyip.com/ yeah, it does exactly what you think it does. Keep in mind it's likely you got
some sort of firewall protecting your external ports, so you'll have to make a rule to let the client through, but that's outside the scope of this document.

As for the port, leave it at 3500. You shouldn't need to reconfigure it anyway. Only possible exception I can think of is if the servers firewall
is set to forward some other external port number to 3500. As of this writing, the server is hardcoded to listen on 3500. The ability to reconfigure the listeningport is probably
coming in the future(yeah it's an oversight).

## Improvements
Just because this is a port, it doesn't mean seamless improvements won't be made. For instance the original client mashed parts of the presentation layer together with the
business logic(mainly in the loginGUI), this wasn't ideal. Among other things it coupled the design of the GUI classes with the Networker(orginally the threadedworker class), 
this time around I'll instead have "platform" that serves as a intermediary between the GUI elements, and the Networker.

I've might very well backport some if not all of these improvements to the old client either at a later date, or simultanously.

Unrelated to the project itself, I've come to learn something new about git: issue tracking. I've not used this future before, instead tracking issues manually. 
This time around I plan to use this functionality.

## Revision history
Some preliminary work had already begun when this was written, so the revision history is somewhat incomplete.

##Critique?

If you see something you think is less than optimal, don't hesitate to let me know, I am always interested to learn to new things.