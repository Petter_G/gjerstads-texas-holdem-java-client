package gholdem;

import static gholdem.protocolEnums.DIV.*;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

//intended to be the same as the original handWidget class, except this one will be private( a notion C++ doesn't support)
public class handWidget extends JPanel
{

    StyledJLabel bet;
    StyledJLabel balance;
    StyledJLabel playername;
    opacityEnabledJLabel leftCard;
    opacityEnabledJLabel rightCard;

    JLabel button;
    JLabel YOULabel;

    JPanel visiblePane;
    JPanel emptyPane;

    CardLayout mainLayout;

    final String visibleID = "1";
    final String emptyID = "2";

    //opacityeffects orginally were declared here. Not sure yet how I am to implement the same functionality in swing, or if it's even possible.
    public handWidget(boolean orientation)
    {

        //initilize components and style them
        bet = new StyledJLabel();
        bet.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        bet.setText("0");
        bet.setHorizontalAlignment(SwingConstants.RIGHT);

        balance = new StyledJLabel();
        balance.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        balance.setText("0");
        balance.setHorizontalAlignment(SwingConstants.RIGHT);

        playername = new StyledJLabel();
        playername.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        //maybe you wonder why I tell the logo to display a string consisting of nothing more but a single unit of empty space? 
        //Turns out the component isn't painted at all if it doesn't have any text currently set.
        playername.setText(" ");

        //Cards and their cardPanel
        leftCard = new opacityEnabledJLabel();
        rightCard = new opacityEnabledJLabel();

        ImageIcon backside = new ImageIcon(getClass().getClassLoader().getResource("resources/images/xx.png"));
        leftCard.setIcon(backside);
        rightCard.setIcon(backside);

        JPanel cardPanel = new JPanel();
        cardPanel.setLayout(new GridBagLayout());

        //create constraints.
        GridBagConstraints cardPanelConstraints = new GridBagConstraints();
        cardPanelConstraints.weightx = 90;

        cardPanelConstraints.gridx = 1;
        cardPanel.add(leftCard, cardPanelConstraints);
        cardPanel.add(Box.createHorizontalStrut(15));
        cardPanelConstraints.gridx = 3;
        cardPanel.add(rightCard, cardPanelConstraints);

        //button and YOU labels. These are not always visible.
        YOULabel = new JLabel();
        button = new JLabel();

        ImageIcon buttonImage = new ImageIcon(getClass().getClassLoader().getResource(RESOURCEBASE + "transparantDealerButton2.png"));
        ImageIcon YOUImage = new ImageIcon(getClass().getClassLoader().getResource(RESOURCEBASE + "transparantYOUlabel.png"));

        button.setIcon(buttonImage);
        YOULabel.setIcon(YOUImage);
        this.toggleYOULabel(false);

        JPanel notificationPanel = new JPanel();
        notificationPanel.setLayout(new GridLayout(1, 2));
        notificationPanel.add(YOULabel);
        notificationPanel.add(Box.createHorizontalStrut(20));
        notificationPanel.add(button);

        //mash it all together.            
        visiblePane = new JPanel();
        visiblePane.setLayout(new GridBagLayout());

        if (orientation == UPPER_PLAYER_ANIMATION)
        {
            GridBagConstraints constraint = new GridBagConstraints();
            constraint.fill = GridBagConstraints.HORIZONTAL;
            constraint.insets = new Insets(1, 0, 1, 0);//int top, int left, int bottom, int right

            constraint.gridy = 1;
            visiblePane.add(playername, constraint);
            constraint.gridy = 2;
            visiblePane.add(balance, constraint);
            constraint.gridy = 3;
            visiblePane.add(bet, constraint);
            constraint.gridy = 4;
            constraint.insets = new Insets(5, 0, 0, 0);
            visiblePane.add(cardPanel, constraint);

            constraint = new GridBagConstraints();
            constraint.gridy = 5;
            constraint.insets = new Insets(5, 0, 0, 0);
            visiblePane.add(notificationPanel, constraint);
        }
        else
        {
            GridBagConstraints constraint = new GridBagConstraints();
            constraint.fill = GridBagConstraints.HORIZONTAL;
            constraint.gridy = 0;
            constraint.insets = new Insets(1, 0, 1, 0);
            visiblePane.add(notificationPanel, constraint);

            constraint.gridy = 1;
            visiblePane.add(playername, constraint);
            constraint.gridy = 2;
            visiblePane.add(balance, constraint);
            constraint.gridy = 3;
            visiblePane.add(bet, constraint);
            constraint.gridy = 4;
            constraint.insets = new Insets(5, 0, 0, 0);
            visiblePane.add(cardPanel, constraint);
        }

        
        //Swing doesn't appear to have a setRetainsSizeWhenHidden() equivalent, so we'll have to get inventive. 
        //Will use a card layout to obtain the same effect.
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));//wanted a bit of space between the widget and other components.
        emptyPane = new JPanel();
        emptyPane.add(Box.createRigidArea(new Dimension(visiblePane.getPreferredSize())));
        
        mainLayout = new CardLayout();
        setLayout(mainLayout);
        add(visiblePane, visibleID);
        add(emptyPane, emptyID);

        mainLayout.show(this, emptyID);
    }
    //this function replaces the setRetainsSizeWhenHidden function from Qt. Nothing equivalent seems to exist.
    @Override
    public void setVisible(boolean state)
    {
        if (state)
        {
            mainLayout.show(this, visibleID);
        }
        else
        {
            mainLayout.show(this, emptyID);
        }
    }

    public void setPlayername(String name)
    {
        playername.setText(name);
    }

    @Override
    public String getName()
    {
        return playername.getText();
    }

    //makes one of the player's cards show a specific card. Originally consisted of two methods, setGFXleftCard and setGFXrightCard.
    public void setGFXCard(int suit, int value, boolean side)
    {
        //one might argue that this test is unnecessary, it's an error that should be impossible.
        //unlike the original, which actually proceded to terminate the program, I'll just return this time.
        if (suit < 0 || suit > 3)//types 0-3 represent card suits.
        {
            System.out.println("Error in setGFXCard: Type corrupt\n");
            return;
        }
        else if (value < 1 || value > 13)
        {
            System.out.println("Error in setGFXCard: Value corrupt.\n");
            return;
        }

        //in order to show the right graphics, we need to know which resource to load, we take advantage of the naming scheme I originally came up with.
        //In short: each card name consists of two numbers: the suit and the value, first number detones the suit, second number denotes the value from 1 to 13.
        String imageURL = RESOURCEBASE;
        imageURL += suit;

        String value_text;
        if (value < 10) //compensate for values below 10, otherwise we'd get cards name x1.gif, troublesome.
        {
            value_text = "0" + value;
        }
        else
        {
            value_text = "" + value;
        }

        imageURL += value_text + ".gif";

        //We are ready, time to load the correct file, and set the leftCard to show it.
        ImageIcon cardImage = new ImageIcon(getClass().getClassLoader().getResource(imageURL));

        if (side == LEFT)
        {
            leftCard.setIcon(cardImage);
        }
        else
        {
            rightCard.setIcon(cardImage);
        }
    }

    //makes both cards show their backside.
    public void showBackside()
    {
        String imageURL = RESOURCEBASE;
        imageURL += "xx.png";
        ImageIcon cardImage = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
        leftCard.setIcon(cardImage);
        rightCard.setIcon(cardImage);
    }

    //Hides the cards by loading the blank.png image.
    //built in method setVisibile(false) will make the layout manager ignore the component's size information altogether, as if it wasn't there. 
    //this "invalidation", as oracle calls it, basically means a message is sent up the hierarchy, and the layout gets repainted with the new sizes.
    public void hideCards()
    {
        String imageURL = RESOURCEBASE;
        imageURL += "blank.png";
        ImageIcon cardImage = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
        leftCard.setIcon(cardImage);
        rightCard.setIcon(cardImage);
    }
    
    public void setBet(int betvalue, int totalbet)
    {
        String text = "Bet: $";
        text += betvalue + "(" + totalbet + ")";
        bet.setText(text);
    }

    public void markBetFolded()
    {
        bet.setText("Folded");
    }

    public void markNameLeaving()
    {
        String current = playername.getText();
        current += " (leaving)";
        playername.setText(current);
    }

    public void markBetAllin()
    {
        bet.setText("Allin");
    }

    public void setBalance(int value)
    {
        String text = "Balance: $" + value;
        balance.setText(text);
    }

    //as name might suggest, toggles the dealer button to either show or not show.
    public void toggleDealerButton(boolean state)
    {
        String imageURL = RESOURCEBASE;
        if (state)
        {
            imageURL += "transparantDealerButton2.png";
            ImageIcon cardImage = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
            button.setIcon(cardImage);
        }
        else
        {
            button.setText("");//replaces QLabel::clear()
            imageURL += "BLANKtransparantDealerButton2.png";
            ImageIcon cardImage = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
            button.setIcon(cardImage);
        }
    }

    public void toggleYOULabel(boolean state)
    {
        YOULabel.setVisible(state);
    }

    //sets the cards to be opaque with a factor defined in protocolEnums.DIV.CARDOPACITY_ON.
    //enables the ability to toggle opacity(ie transparancy) on or off with ease.
    public void SetAlpha(boolean state)
    {
        if (state)
        {
            leftCard.setAlpha(CARDOPACITY_ON);
            rightCard.setAlpha(CARDOPACITY_ON);
        }
        else
        {
            leftCard.setAlpha(CARDOPACITY_OFF);
            rightCard.setAlpha(CARDOPACITY_OFF);
        }
    }

}
