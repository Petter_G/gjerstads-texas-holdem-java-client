package gholdem;

import static gholdem.protocolEnums.CLIENTENUMS.*;
import gholdem.protocolEnums.DIV;
import static gholdem.protocolEnums.SERVERCOMMANDS.*;
import static gholdem.protocolEnums.DIV.*;
import static gholdem.protocolEnums.MessageEnums.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Networker
{
    Socket ServConn;
    
    OutputStream out;
    InputStream in;
        
    final platform platform;
    
    Thread dataProcessor;
    
    public Networker(platform parent)
    {
        platform = parent;
    }
    
    //true means it was successful, false otherwise
    private boolean ConnectToServer(String serverIP,int port)
    {        
        try
        {
            //Decided to simply reinitialize the socket with each connection attempt. 
            //Since the isConnected method merely reports if the socket has been connected at some point, not if it's currently connected, this is the most convenient method.
            ServConn = new Socket();
            ServConn.connect(new InetSocketAddress(serverIP,port),CONNECT_TIMEOUT );
            
            out = ServConn.getOutputStream();
            in = ServConn.getInputStream();
                
                //OBS: Eksperimenter med buffered readers etterhvert.
            /*InputStreamReader reader = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(reader);*/
            
            //Note: the dataProcessor will run in its own thread. This thread will be run continously, 
            //ergo it cannot be done in the executor that exists solely for use by the platform as a means to 
            //offload potentially heavy tasks and as such needs to be available.
            dataProcessor = new Thread(new ReadHandler(ServConn, in));
            dataProcessor.setName("data processor");//for debugging purposes
            dataProcessor.setDaemon(true);
            dataProcessor.start();

            System.out.println("Connection succeeded");
            return true;
        }
        catch( ConnectException e )
        {
            //this functionality was originally implemented in the loginGUI in the form of displayError(), decided to move it here as these exceptions
            //are tied to connection state rather than problems with the user not being able to enter the correct login/register information.
            JOptionPane.showMessageDialog(null, "Unable to connect, please make sure your connection details are correct.");
        }
        catch ( SocketTimeoutException e )
        {
            JOptionPane.showMessageDialog(null,"Connection attempt timed out, please make sure your connection details are correct.");
        }
        catch(UnknownHostException e)//if the hostname makes no sense on the target network, this happens.
        {
            JOptionPane.showMessageDialog(null,"Connection failed. Host is unreachable.");
        }
        catch(IOException ex)
        {
            JOptionPane.showMessageDialog(null,"IO exception: "+ ex.toString() );
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,"generic error: "+ e.toString() );
        }
        
        ServConn = null;
        return false;
    }
    
    //
    public void disconnectFromServer()
    {
        try
        {
            out.close();
            in.close();
            ServConn.close();
            ServConn = null;
            
            //also null out dataProcessor
            dataProcessor = null;
            
            //We'll need to tell the GUIs to reset all their fields, hide the cards and such.
            SignalGUI_resetYourselfRunner runner = new SignalGUI_resetYourselfRunner(platform);
            javax.swing.SwingUtilities.invokeLater(runner);
        }
        catch (IOException ex)
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //sends the message, ie pushes the message on to the socket.
    public void writeMessageToSocket(String text)
    {
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            int[] temp = new int[1];
            temp[0] = SHORTCHAT;
            
            stream.write(intArrayToByteArray(temp));//opcode written
            
            for (int i = 0; i < text.length(); i++)
            {
                temp[0] = text.charAt(i);
                stream.write(intArrayToByteArray(temp) );
            }
            //ready for transmission.
            out.write(stream.toByteArray());
        }
        catch (IOException ex)
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void writeDisplayNameChangeRequestToSocket(String name)
    {
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            int[] temp = new int[1];
            temp[0] = SETNAME;
            
            stream.write(intArrayToByteArray(temp));//opcode written
            
            for (int i = 0; i < name.length(); i++)
            {
                temp[0] = name.charAt(i);
                stream.write(intArrayToByteArray(temp) );
            }
            out.write(stream.toByteArray() );
        }
        catch (IOException ex)
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void genericCommandReceiver(int opcode,int value1,int value2, int value3,String str1,String str2)
    {
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            int[] opcodeArray = new int[1];
            int[] value1Array = new int[1];
            int[] value2Array = new int[1];
            int[] value3Array = new int[1];
            
            int[] variableIntArray;
            
            switch (opcode)
            {
                case CALL:
                {
                    opcodeArray[0] = CALL;
                    stream.write(intArrayToByteArray(opcodeArray) );
                    break;
                }
                case ALLIN:
                {
                    opcodeArray[0] = ALLIN;
                    stream.write(intArrayToByteArray(opcodeArray) );
                    break;
                }
                case FOLD:
                {
                    opcodeArray[0] = FOLD;
                    stream.write(intArrayToByteArray(opcodeArray) );
                    break;
                }
                case LEAVEROOM:
                {
                    opcodeArray[0] = LEAVEROOM;
                    stream.write(intArrayToByteArray(opcodeArray) );
                    break;
                }
                case RAISE:
                {
                    
                    opcodeArray[0] = RAISE;
                    value1Array[0] = value1;
                    stream.write(intArrayToByteArray(opcodeArray));
                    stream.write(intArrayToByteArray(value1Array));
                    break;
                }
                case REFILL:
                {
                    opcodeArray[0] = REFILL;
                    value1Array[0] = value1;
                    stream.write(intArrayToByteArray(opcodeArray));
                    stream.write(intArrayToByteArray(value1Array));
                    break;
                }
                case TRANSFER:
                {
                    opcodeArray[0] = TRANSFER;
                    value1Array[0] = value1;
                    stream.write(intArrayToByteArray(opcodeArray));
                    stream.write(intArrayToByteArray(value1Array));
                    break;
                }
                case JOIN://opcode-gameid-usingPSW-pswCount-psw
                {
                    //mandatories
                    opcodeArray[0] = JOIN;
                    value1Array[0] = value1;
                    value2Array[0] = value2;//usingPSW
                    
                    //optional
                    value3Array[0] = value3;//pswCount
                    
                    //write mandatories first
                    stream.write(intArrayToByteArray(opcodeArray));
                    stream.write(intArrayToByteArray(value1Array));
                    stream.write(intArrayToByteArray(value2Array));
                    
                    //now write optionals, if usingPSW, ie value2, is true
                    if ( value2 == DIV.INTEGERTRUE)
                    {
                        stream.write(intArrayToByteArray(value3Array));//pswCount
                        this.writeStringToSocket(stream, str1);//psw
                    }
                    break;
                }                
                case CREATE:
                {
                    opcodeArray[0] = CREATE;
                    
                    value1Array[0] = str1.length();//length of username
                    value2Array[0] = str2.length();//length of password
                    
                    //if str2(password) is length 0, then pswprot is false
                    if ( str2.length() == 0 )
                        value3Array[0] = 0;//false is logically the value 0;
                    else
                        value3Array[0] = 1;//obviously then, 1 is the value for true
                    
                    stream.write(intArrayToByteArray(opcodeArray) );
                    stream.write(intArrayToByteArray(value3Array) );
                    stream.write(intArrayToByteArray(value1Array) );
                    writeStringToSocket(stream, str1);
                    
                    //if optional password is set, protocol allows for this extension
                    if (str2.length() != 0)
                    {
                        stream.write(intArrayToByteArray(value2Array) );
                        this.writeStringToSocket(stream,str2);
                    }
                    break;
                }
                default:
                {
                    System.out.println("Error: opcode(" + opcode + ") is unknown. Ignoring.");
                }
            }
            out.write(stream.toByteArray());
        }
        catch (IOException ex)
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public void doProfile(String username,String password,boolean logintype)
    {
        //doProfile is the only method that can be invoked before a connection has been established. 
        //Ergo we have to check whether a connection has been made or not, and if so make that connection first.
        //As a sidenote, the isConnected method is poorly named. It merely reports if the socket has been connected at some point, not it's current state.
        //connection issues are not handled automatically by the API.
        //Currently the connectToServer method will reinitialize the socket, meaning that the isconnected can be relied upon to tell us if the socket 
        //has already been successfully connected or not. Still won't do anything to detect random disconnects however, that'll have to come later.
        if ( ServConn == null )//no active connection.
        {
            //now to check if the connection attempt was successful. if not return.
            if ( !ConnectToServer(ConfigHandler.getIPTarget(),ConfigHandler.getPortTarget() ) )
                return;
        }

        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            int[] temparray = new int[1];
            
            //we need to deduce whether this is a login attempt or a register new profile attempt.
            if (logintype)
                temparray[0] = LOGIN;
            else
                temparray[0] = REGISTER;
            
            stream.write(intArrayToByteArray(temparray) );//writes the opcode
            temparray[0] = username.length();
            stream.write(intArrayToByteArray(temparray) );//writes the number of characters in the username
            temparray[0] = password.length();
            stream.write(intArrayToByteArray(temparray) );//same just for the password field.

            //create a int array, use our intArrayToByteArray to convert that array to the correct format, then push to stream.
            writeStringToSocket(stream,username);
            
            //repeat for password
            writeStringToSocket(stream,password);
            
            out.write(stream.toByteArray());
        }
        catch (IOException ex)
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }


    //What follows from here are private methods used for various tasks.    
    //To my annoyance, outputstreams don't take int arrays directly, you have to convert them to byte arrays of size 4 first.
    private byte[] intArrayToByteArray(int[] arr)
    {
        ByteBuffer buffer;
        byte[] barr = new byte[arr.length*4];
        
        for (int i = 0; i < arr.length; i++)
        {
            buffer = ByteBuffer.allocate(4);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.putInt( arr[i] );
            barr[(i*4)+0] = buffer.get(0);
            barr[(i*4)+1] = buffer.get(1);
            barr[(i*4)+2] = buffer.get(2);
            barr[(i*4)+3] = buffer.get(3);
        }
        return barr;
    }
    
    //Helper function, lets us push an entire string to the socket in the right format. Recall that each bit of data we send according to my protocol, will take up 4 bytes.
    //Individual characters for instance will always take up 4 bytes. While inefficient, it's reasonably simple to work with. The protocol did not originally intend to support UTF8, but it 
    //does look like support was added by accident, after all 4 bytes is the maximum size a UTF8 character can be. Norwegian characters seem to be transmitted, and rendered, without problems.
    private void writeStringToSocket(ByteArrayOutputStream stream,String str)
    {
        //create a int array, use our intArrayToByteArray to convert that array to the correct format, then push to stream.
        int[] temparray = new int[ str.length() ];
        
        for (int i = 0; i < str.length(); i++)
            temparray[i] = (int) str.charAt(i);
        
        try 
        {
            stream.write(intArrayToByteArray(temparray));
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Used by dataProcessor
    private class ReadHandler implements Runnable
    {
        private final Socket ServConn;
        private final InputStream in;
        
        public ReadHandler(Socket connRef, InputStream inRef)
        {
            ServConn = connRef;
            in = inRef;
        }
        
        private int createIntFromByteArray(byte[] array)
        {
            ByteBuffer buffer = ByteBuffer.wrap(array);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int data = buffer.getInt();
            return data;
        }
        
        //Convenience method. Reads n blocks, each block being exactly 4 bytes, packs these ints into an array and returns them.
        private int[] BlockReader(int blockCount)
        {
            byte[] buffer = new byte[4];
            int[] data = new int[blockCount];
            
            for (int i = 0; i < blockCount; i++)
            {
                try
                {
                    in.read(buffer);
                    data[i] = createIntFromByteArray(buffer);
                }
                catch (IOException ex)
                {
                    Logger.getLogger(Networker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return data;
        }
            
        @Override
        public void run()
        {
            
            int count = 0;
            try
            {
                /*as a note, I discovered that finding out whether a connection is valid or not, is not as straightforward as you'd think.
                Since checking of the address is valid is not the same as checking if a connection is valid as the host and server are different entities.
                For instance, if the server is forcibly shut down on the host, then any resources associated with the host is freed 
                automatically by the hosts operating system, and here is the important bit: The OS then tells any clients connected 
                that their connections are finilized via the TCP protocol.
                if the host itself crashes however, no such message goes out, and the clients will just sit there stuck in their read calls.
                Checking the count variable for -1 will essentially mean detecting if the finilize message has been received. 
                IE, a server crash will not be picked up at all.              
                */
                while ( count != -1 ) //this while loop is this port's equivalent to incomingData()
                {
                    byte[] buffer = new byte[4];
                    byte[] opcodeArray = new byte[4];
                    
                    //debugging, will be kept in place for development purposes.
                    System.out.println("BEFORE READ");
                    count = in.read(opcodeArray);
                    System.out.println("bytes read: " + count );

                    if (count == -1)
                    {
                        /*Might have to explain this one. If the other side, ie the server, closed down the connection, then read() will return
                        -1, which is code for "connection closed".

                        Basically this means two things: First we schedule a DisconnectFromServerRunner, then we return. That way the dataProcessor 
                        thread, ie the thread we are currently in, returns, and thus ends. 
                        which is what we want. Later on if the player decides to reconnect, then the networker
                        is in the same state it was when first created.*/
                        platform.getExecutor().execute(new DisconnectFromServerRunner(platform.getNetworker() ));
                        return;
                    }
                    
                    int opcode = createIntFromByteArray(opcodeArray);
                    System.out.println("Opcode is: "+opcode);
                    
                    switch ( opcode )
                    {
                        case PLAYERSTATECHANGEDNOTIFY:
                        {
                            in.read(buffer);
                            int serversidefd = createIntFromByteArray( buffer );
                            in.read(buffer);
                            int gameid = createIntFromByteArray( buffer );
                            in.read(buffer);
                            int dcstate  = createIntFromByteArray( buffer );
                            
                            dataContainer container = new dataContainer_three(opcode,serversidefd,gameid,dcstate);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case PLAYERLEFTLOCALROOMNOTIFY:
                        {
                            in.read(buffer);
                            int tableslot = createIntFromByteArray( buffer );
                            
                            dataContainer container = new dataContainer_one(opcode,tableslot);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case PLAYERJOINEDROOMNOTIFY:
                        {                            
                            in.read(buffer);
                            int serversidefd = createIntFromByteArray( buffer );
                            in.read(buffer);
                            int gameid = createIntFromByteArray( buffer );
                            
                            dataContainer container = new dataContainer_two(opcode,serversidefd,gameid);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container));
                            break;
                        }
                        case JOINFAILURENOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case JOINSUCCESSNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case CLIENTDISSCONNECTEDNOTIFY:
                        {
                            int serversidefd;
                            int gameid;
                            
                            in.read(buffer);
                            serversidefd = createIntFromByteArray(buffer);
                            in.read(buffer);
                            gameid = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_two(opcode,serversidefd,gameid);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case NEWPLAYERNOTIFY:
                        {                            
                            in.read(buffer);
                            int gameid = createIntFromByteArray(buffer);
                            in.read(buffer);
                            int serversidefd = createIntFromByteArray(buffer);
                            in.read(buffer);
                            int messagelength = createIntFromByteArray(buffer);
                            
                            int[] data = BlockReader(messagelength);
                            
                            dataContainer container = new dataContainer_twoSpecial_intArray(opcode,gameid,serversidefd,data);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case NEWGAMENOTIFY:
                        {//opcode-gameid-roomnameCount-roomname
                            int id;
                            int roomnameCount;
                            
                            in.read(buffer);
                            id = createIntFromByteArray(buffer);
                            in.read(buffer);
                            roomnameCount = createIntFromByteArray(buffer);
                            
                            int[] roomNameData = BlockReader( roomnameCount );
                            StringBuilder sb = new StringBuilder();
                            
                            for (int i = 0; i < roomnameCount; i++)
                            {
                                sb.append( (char)roomNameData[i] );
                            }
                            
                            dataContainer container = new dataContainer_twoSpecial_intArray(opcode,id,roomnameCount,roomNameData );
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case AUTHENTICATIONSUCCESSNOTIFY:
                        {
                            int cookie;
                            in.read(buffer);
                            cookie = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,cookie);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case AUTHENTICATIONFAILURENOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDPROFILECREATION:
                        {                            
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case VALIDPROFILECREATION:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case SERVERSHUTDOWNNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case REPAINT:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case PLAYERSNOTREADY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case TABLESLOTNOTIFY:
                        {
                            int tableslot;
                            int trulyNew;
                            int local;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            in.read(buffer);
                            trulyNew = createIntFromByteArray(buffer);
                            in.read(buffer);
                            local = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_three(opcode,tableslot,trulyNew,local);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDCOMMAND:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case VALIDCOMMAND:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case NAMECHANGEDNOTIFY:
                        {
                            in.read(buffer);
                            int serversidefd = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int silent = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int tableslot = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int messagelength = this.createIntFromByteArray(buffer);
                            
                            int[] data = BlockReader( messagelength );
                            
                            String newname;
                            StringBuilder sb = new StringBuilder();
                            
                            for (int i = 0; i < messagelength; i++)
                            {
                                sb.append( (char)data[i] );
                            }
                            newname = sb.toString();
                            
                            dataContainer container = new dataContainer_four_Special_StringArray(opcode,serversidefd,silent,tableslot,messagelength,newname);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case TABLESLOTCHANGEDNAMENOTIFY:
                        {
                            in.read(buffer);
                            int tableslot = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int silent = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int namelength = this.createIntFromByteArray(buffer);
                            
                            int[] data = BlockReader( namelength );
                            
                            dataContainer container = new dataContainer_twoSpecial_intArray(opcode,tableslot,silent,data);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );                            
                            break;
                        }
                        case NAMECHANGEINVALIDNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case HANDUPDATE:
                        {
                            in.read(buffer);
                            int tableslot = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int card1 = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int card2 = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int card3 = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int card4 = this.createIntFromByteArray(buffer);
                            
                            
                            dataContainer container = new dataContainer_five(opcode,tableslot,card1,card2,card3,card4);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container));
                            break;
                        }
                        case DELETEREP:
                        {
                            int tableslot;
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer) ;
                            
                            dataContainer container = new dataContainer_one(opcode,tableslot);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case BETUPDATE:
                        {
                            int tableslot;
                            int bet;
                            int totalbet;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            in.read(buffer);
                            bet = createIntFromByteArray(buffer);
                            in.read(buffer);
                            totalbet = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_three(opcode,tableslot,bet,totalbet);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case BALANCEUPDATE:
                        {
                            int tableslot;
                            int balance;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            in.read(buffer);
                            balance = createIntFromByteArray(buffer);
                            
                            
                            dataContainer container = new dataContainer_two(opcode,tableslot,balance);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case ACTIVEUPDATE:
                        {
                            int tableslot;
                            int activestate;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            in.read(buffer);
                            activestate = this.createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_two(opcode,tableslot,activestate);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case ALLINUPDATE:
                        {
                            int tableslot;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,tableslot);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case NAMETOLONGNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case TABLERESET:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case COMMUNITYCARDADDED:
                        {
                            int suit;
                            int rank;
                            
                            in.read(buffer);
                            suit = createIntFromByteArray(buffer);
                            in.read(buffer);
                            rank = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_two(opcode,suit,rank);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case POTNOTIFY:
                        {
                            int pot;
                            
                            in.read(buffer);
                            pot = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,pot);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case CURRENTPLAYERNOTIFY:
                        {
                            int newCurrentPlayer;
                            
                            in.read(buffer);
                            newCurrentPlayer = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,newCurrentPlayer);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case CURRENTBUTTONNOTIFY:
                        {
                            int currentButton;
                            
                            in.read(buffer);
                            currentButton = this.createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,currentButton);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case MINIMUMRAISEFAILURENOTIFY:
                        {
                            int bigblind;
                            
                            in.read(buffer);
                            bigblind = this.createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,bigblind);
                            javax.swing.SwingUtilities.invokeLater( new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case PAUPERFAILURENOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case CHECKINVALIDNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case NOTYOURTURNNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case REJECTCONNECTIONNOTIFY:
                        {
                            //obsolete?
                            break;
                        }
                        case GAMEHALTEDNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case GAMEPAUSEDNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case WINNERINFONOTIFY:
                        {
                            //as a note, the original implementation had the poorly named "painter" own the winnerInfo objects. Now I've moved
                            //the ownage to holdem, which makes a ton more sense.
                            in.read(buffer);
                            int winnertableslot = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int payout = this.createIntFromByteArray(buffer);
                            in.read(buffer);
                            int potIndex = this.createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_three(opcode,winnertableslot,payout,potIndex);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case MAXROOMSCREATEDNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDROOMNAMELENGTHNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDROOMPSWLENGTHNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDROOMPSWNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case PSWPROTECTEDNOTIFY:
                        {
                            //load the gameid, pass it on to the lobby
                            in.read(buffer);
                            int gameid = createIntFromByteArray(buffer);
                            
                            dataContainer container = new dataContainer_one(opcode,gameid);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case INVALIDROOMNAMEEXISTSNOTIFY:
                        {
                            dataContainer container = new dataContainer(opcode);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        case CHAT:
                        {
                            int tableslot;
                            int fd;
                            int length;
                            
                            in.read(buffer);
                            tableslot = createIntFromByteArray(buffer);
                            in.read(buffer);
                            fd = createIntFromByteArray(buffer);
                            in.read(buffer);
                            length = createIntFromByteArray(buffer);
                            
                            int[] message = BlockReader(length);
                            dataContainer container = new dataContainer_twoSpecial_intArray(opcode,tableslot,fd,message);
                            javax.swing.SwingUtilities.invokeLater(new GUISchedulingRunner(platform,container) );
                            break;
                        }
                        default://if we ever get here, I either forgot to implement support for a given opcode, or something outside of my control failed.
                        {
                            int error;
                            System.out.println("Error: Pipe corrupted, dumping raw data(by blocks of four bytes):");
                            System.out.println("Opcode: "+opcode);
                            
                            while ( in.available() != 0)
                            {
                                int value;
                                in.read(buffer);
                                value = createIntFromByteArray(buffer);
                                System.out.println(" " + value);
                            }
                            break;
                        }
                    }
                
                }
            }
            catch (SocketException se)
            {
                System.out.println("Socket exception detected: "+se.toString() );
            }
            catch ( IOException IOE )
            {
                System.out.println("IO exception detected: "+IOE.toString());
            }
        }
    }//end of ReadHandler

}// end of Networker
