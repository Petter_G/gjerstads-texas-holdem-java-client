package gholdem;

import static gholdem.protocolEnums.CLIENTENUMS.*;
import gholdem.protocolEnums.DIV;
import static gholdem.protocolEnums.SERVERCOMMANDS.*;
import static gholdem.protocolEnums.DIV.*;
import static gholdem.protocolEnums.MessageEnums.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.TrayIcon;
import java.awt.Window;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.border.BevelBorder;

import javax.swing.tree.*;
    
public class lobbyGUI extends JPanel
{
    private platform platform;
    private JMenuBar menuBar;
    private JMenu menu;
    private JLabel logo;
    private JButton join;
    private JButton create;
    
    private JTree roomTree;
    private DefaultTreeModel roomModel;
    private DefaultMutableTreeNode roomRootNode;
    private PersonalizedJToggleButton roomSortButton;
    
    private JTree lobbyerTree;
    private DefaultMutableTreeNode lobbyerRootNode;
    private DefaultTreeModel lobbyerModel;
    private PersonalizedJToggleButton loungerSortButton;
    
    private AutoDatingJTextArea chatArea = new AutoDatingJTextArea();
    private JTextField chatInput = new JTextField();
    
    private DefaultMutableTreeNode selectedNode;
        
    public lobbyGUI(platform platformref)
    {
        super(new GridLayout(1,0));
        platform = platformref;
        //menu bar
        menuBar = new JMenuBar();
        menu = new JMenu("User");
        menu.setMnemonic( KeyEvent.VK_U );
        
        JMenuItem changeNameMenuItem = new JMenuItem("Change name");
        changeNameMenuItem.addActionListener( (ActionEvent e) -> { onNameChange(); } );
        changeNameMenuItem.setMnemonic( KeyEvent.VK_C );
        
        JMenuItem logoutMenuItem = new JMenuItem("logout");
        logoutMenuItem.addActionListener( (ActionEvent e) -> { onLogout(); } );
        logoutMenuItem.setMnemonic( KeyEvent.VK_L );
        
        menu.add(changeNameMenuItem);
        menu.add(logoutMenuItem);
        
        menuBar.add(menu);
        //pack it into a jpanel
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout( new BorderLayout() );
        menuPanel.add(menuBar,BorderLayout.NORTH);
        
        //logo
        ImageIcon logoImage = new ImageIcon(getClass().getClassLoader().getResource("resources/images/transparantLogo.png") );
        logo = new JLabel();
        logo.setIcon(logoImage);
        logo.setPreferredSize(new Dimension( (int)(logoImage.getIconWidth()* 1.05f),logoImage.getIconHeight() * 3 ) );
        logo.setHorizontalAlignment(SwingConstants.CENTER);
        
        JPanel logoPanel = new JPanel();
        logoPanel.setLayout(new GridLayout(1,1) );
        logoPanel.setBackground(Color.green.darker());
        logoPanel.add(logo);
        
        //join and create buttons
        join = new JButton("Join");
        join.setPreferredSize(new Dimension(80,join.getPreferredSize().height));
        join.addActionListener((ActionEvent e) -> 
        { 
            onJoin();  
            takeFocus();
        });
        create = new JButton("Create");
        create.setPreferredSize(new Dimension(80,create.getPreferredSize().height));
        create.addActionListener((ActionEvent e) -> 
        { 
            onCreate();
            takeFocus();
        });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout( new GridBagLayout() );
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = GridBagConstraints.NORTH;
        buttonPanel.add(join,cons );
        buttonPanel.add(Box.createHorizontalStrut(10),cons);
        buttonPanel.add(create,cons );
        
        JPanel buttonPanelWrapper = new JPanel( ); 
        buttonPanelWrapper.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        buttonPanelWrapper.setLayout( new GridBagLayout() );
        cons = new GridBagConstraints();
        cons.gridheight = 30;
        buttonPanelWrapper.add(buttonPanel,cons);
        buttonPanelWrapper.setMaximumSize(new Dimension(250,20));
        
        //JTreeHeader        
        JLabel roomTreeLabel = new JLabel("Available tables",SwingConstants.LEFT);
        JLabel loungerTreeLabel = new JLabel("Lounging players",SwingConstants.RIGHT);
        
        roomSortButton = new PersonalizedJToggleButton();
        loungerSortButton = new PersonalizedJToggleButton();
        
        roomSortButton.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if ( roomSortButton.isSelected() )
                    onSortRooms(true);
                else
                    onSortRooms(false);
                takeFocus();
            }
        });
        loungerSortButton.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if ( loungerSortButton.isSelected() )
                    onSortLoungers(true);
                else
                    onSortLoungers(false);
                takeFocus();
            }
        });

        JPanel header = new JPanel( new GridBagLayout() );
        header.setMaximumSize(new Dimension(2000,100));
        //header.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        header.setBorder(BorderFactory.createBevelBorder( BevelBorder.RAISED, Color.BLACK, Color.lightGray));
        cons = new GridBagConstraints();
        cons.weightx = 0.1;
        GridBagConstraints spaceCons = new GridBagConstraints();
        spaceCons.fill = GridBagConstraints.HORIZONTAL;
        spaceCons.weightx = 0.5;
        
        cons.anchor = GridBagConstraints.WEST;
        header.add(roomTreeLabel,cons);
        header.add(new JPanel(),spaceCons);
        cons.anchor = GridBagConstraints.EAST;
        header.add(roomSortButton,cons);
        cons.anchor = GridBagConstraints.WEST;
        header.add(loungerSortButton,cons);
        header.add(new JPanel(),spaceCons);
        cons.anchor = GridBagConstraints.EAST;
        header.add(loungerTreeLabel,cons);
            
        //Jtree and associated entities.
        roomRootNode = new DefaultMutableTreeNode();
        roomTree = new JTree(roomRootNode);
        roomTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        roomTree.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER )//attempt to join, almost the same as a doubleclick.
                    onJoin();
                else if ( e.getKeyCode() == KeyEvent.VK_ESCAPE )//if user presses escape, return focus to the lobbyGUI
                    takeFocus();
            }
        });
        
        //I want to support doubeclicking like in the c++ version, here I'll have to register a mouseadapter.
        //Might add that this bit of code is the reimplementation of the onItemSelected and onItemDoubleClick functions.
        MouseListener mouse = new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                int selRow = roomTree.getRowForLocation(e.getX(), e.getY());
                TreePath path = roomTree.getPathForLocation(e.getX(), e.getY());

                if (selRow != -1)//IE a node has been seleceted and not white space.
                {
                    //update selectedNode for later use
                    selectedNode = (DefaultMutableTreeNode)path.getLastPathComponent();
                    
                    //if doubleclick, call onJoin()
                    if (e.getClickCount() == 2)
                        onJoin();
                }
                else//ie whitespace
                    takeFocus();
            }
        };
        
        roomTree.addMouseListener(mouse);
        roomTree.setRootVisible(false);
        roomTree.setShowsRootHandles(true);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer)roomTree.getCellRenderer();
        renderer.setLeafIcon(null);   
        
        //initialize roomnode for later use
        roomModel = (DefaultTreeModel)roomTree.getModel();
        JScrollPane roomTreeScroller = new JScrollPane(roomTree);
        
        //now for the other tree which will hold players who are "lounging", ie just sitting around in the lobby.
        lobbyerRootNode = new DefaultMutableTreeNode();
        lobbyerTree = new JTree(lobbyerRootNode);
        lobbyerTree.setRootVisible(false);
        lobbyerTree.setShowsRootHandles(true);
        
        renderer = (DefaultTreeCellRenderer)lobbyerTree.getCellRenderer();
        renderer.setLeafIcon(null);      
        lobbyerModel = (DefaultTreeModel)lobbyerTree.getModel();
        JScrollPane lobbyerTreeScroller = new JScrollPane(lobbyerTree);
        
        //treeView will hold our two trees,
        JPanel treeView = new JPanel(new GridLayout(1,2) );
        treeView.add(roomTreeScroller);
        treeView.add(lobbyerTreeScroller);
        
        //chatarea
        chatArea = new AutoDatingJTextArea();
        chatArea.setEditable(false);
        chatArea.setLineWrap(true);
        chatArea.setWrapStyleWord(true);
        
        JScrollPane chatareapane = new JScrollPane(chatArea);
        chatareapane.setPreferredSize(new Dimension(0,200));
        chatareapane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        
        //inputfield
        chatInput = new JTextField(20);
        chatInput.setMaximumSize(new Dimension(40000,20));// does the trick, even though it's hackish.
        chatInput.addActionListener( (ActionEvent e) -> { onInput(); });
        chatInput.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                //simply put, if we press enter when the chatInput component has focus, onInput needs to be called. onInput will make sure 
                //to return focus to the lobbyGUI.
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )
                    onInput();
            }
        });
        
        //we need to register some bindings for what to do when the lobbyGUI itself has focus. It's simple, a press of enter should send the focus
        //to the chatInput component, same as if you mouseclicked it, other keyboard shortcuts will be set with mnemonics system instead.
        addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )
                    chatInput.requestFocus();
            }
        });

        //layouts        
        
        JPanel outer = new JPanel();
        outer.setLayout(new BoxLayout(outer,BoxLayout.Y_AXIS) );
        outer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        outer.add(logoPanel);
        outer.add(buttonPanelWrapper);
        outer.add(header);
        outer.add(treeView);
        outer.add(Box.createVerticalStrut(10));
        outer.add(chatareapane);
        outer.add(Box.createVerticalStrut(10));
        outer.add(chatInput);
        
        setLayout(new BorderLayout() );
        add(menuPanel,BorderLayout.NORTH);
        add(outer);
        
    }
    
    //simple callback that enables components outside of this scope to surrender their focus.
    public void takeFocus()
    {
        requestFocusInWindow();
    }
    
    //pretty straight forward what this one does, resets all the mutable element of the lobbyGUI.
    public void clearGUI()
    {
        chatInput.setText("");
        roomRootNode.removeAllChildren();
        lobbyerRootNode.removeAllChildren();
        roomModel.reload();
        chatArea.setText("");
    }
    
    
    /*The original implementation had a set of overloaded genericReceivers as well as the chatreceiver. 
    I didn't go with a solution like this one because I focused blindly on optimal performance, 
    something which is unproblematic in this simple GUI anyway. 
    The original design led to an increase in complexity without truly reducing verbosity, in fact adding to it.
    this time I've decided to just live with the unused arguments.*/
    //public void genericReceiver(int[] info, String statement, int opcode, int[] extra, int value1, int value2, int value3)
    public void genericReceiver(dataContainer container)
    {
        switch ( container.getOpcode() )
        {
            case GAMEPAUSEDNOTIFY:
            {
                if ( !(container instanceof dataContainer) )
                    throw new IllegalStateException("container supplied is not of type dataContainer");
                
                dataContainer ref = container;//technically unnessesary, added for consistency.
                this.chatArea.append("The server is still processing your last play, please wait.");
                break;
            }
            case NEWPLAYERNOTIFY:
            {
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                
                String name;
                int gameid = ref.getValue1();
                int serversidefd = ref.getValue2();
                
                StringBuilder builder = new StringBuilder();
                for ( int letter : ref.getData()  )
                {
                    builder.append( (char)letter);
                }
                name = builder.toString();
                
                //This opcode is arguably poorly named, as it pertains not only to players who just joined the server, 
                //but players who were present before this client connected. Also discovered that the original implementation was especially poor.
                DefaultMutableTreeNode node = new DefaultMutableTreeNode( new userData(name,gameid,serversidefd) );
                
                //now, if the gameid is not -1, then we need to find that would-be parentnode first.
                if (gameid != -1)
                {
                    DefaultMutableTreeNode parentnode = findNode(gameid, ROOMNODE);
                    parentnode.add(node);
                    roomModel.reload();
                }
                else
                {
                    lobbyerRootNode.add(node);
                    lobbyerModel.reload();
                }
                break;
            }
            case NAMECHANGEDNOTIFY:
            {
                if ( !(container instanceof dataContainer_four_Special_StringArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_four_Special_StringArray");
                
                dataContainer_four_Special_StringArray ref = (dataContainer_four_Special_StringArray)container;
                int serversidefd = ref.getValue1();
                String newName = ref.getData();
                
                //Find the node in question, store the current username, update to the new one, reload the model. Append chat area with notice.
                DefaultMutableTreeNode node = findNode(serversidefd,USERNODE);
                String oldName = ((userData)node.getUserObject()).getUserAlias();
                ((userData)node.getUserObject()).setUserAlias(newName);
                roomModel.reload();
                lobbyerModel.reload();
                
                //also make sure a notification is played.
                chatArea.append("'"+ oldName + "' is now known as '" + newName+"'");
                break;
            }
            case PLAYERSTATECHANGEDNOTIFY:
            {
                if ( !(container instanceof dataContainer_three) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_three");
                
                dataContainer_three ref = (dataContainer_three)container;
                
                int serversidefd = ref.getValue1();
                int roomid = ref.getValue2();
                boolean dcstate = ref.getValue3() != 0 ? true : false;
                
                //first we find the player node that has changed state, and save a reference to it.
                DefaultMutableTreeNode node = findNode(serversidefd,USERNODE);
                
                //now we fetch that node's parent, and then ask that parent to delete its childnode, 
                //the same one we still hold a reference to external to the tree.
                DefaultMutableTreeNode parentnode = (DefaultMutableTreeNode)node.getParent();
                parentnode.remove(node);

                //Next we need to figure out whether the node is to be readded to the tree or not
                //if dcstate is false, then the client is still connected, if not then the player left the server altogether, and is thus not readded 
                //to the model.
                if (dcstate == false)
                    lobbyerRootNode.add(node);
                roomModel.reload();
                lobbyerModel.reload();
                break;
            }
            case PLAYERJOINEDROOMNOTIFY:
            {
                if ( !(container instanceof dataContainer_two) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_two");
                
                dataContainer_two ref = (dataContainer_two)container;
                
                int serversidefd = ref.getValue1();
                int roomid = ref.getValue2();
                
                //First we need to find the user, who is currently lounging, store a reference of that node outside the tree,
                //then remove that player.
                //We then track down the right room node, and add the saved lobbyer node to that room node.
                DefaultMutableTreeNode lounger = findNode(serversidefd,USERNODE);
                lobbyerRootNode.remove(lounger);
                DefaultMutableTreeNode roomNode = findNode(roomid,ROOMNODE);
                roomNode.add(lounger);
                roomModel.reload();
                lobbyerModel.reload();
                break;
            }
            case NEWGAMENOTIFY:
            {
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                int gameid = ref.getValue1();
                int roomNameCount = ref.getValue2();
                int roomNameData[] =  ref.getData();
                
                String roomName;
                StringBuilder SB = new StringBuilder();
                for ( int i = 0; i < roomNameData.length; i++ )
                    SB.append( (char)roomNameData[i] );
                
                roomName = SB.toString();
                //simple enough, create a new mutableNode and a roomData object, make the new node take posession of the object, 
                //and append it to the rootnode.
                roomRootNode.add(new DefaultMutableTreeNode(new roomData( gameid,roomName ) ) );
                roomModel.reload();
                break;
            }
            case NAMETOLONGNOTIFY:
            {
                chatArea.append("Your requested new username was to long. Make sure usernames are no more than "+protocolEnums.DIV.MAX_NAMELENGTH
                + " characters in length.");
                break;
            }
            case NAMECHANGEINVALIDNOTIFY:
            {
                chatArea.append("Name change invalid. The username you requested is probably already in use by someone else or illegal, "
                    + "please try again.");
                break;
            }
            case JOINFAILURENOTIFY:
            {
                chatArea.append("You are still technically active in another room, you will not "+
                    "be able to join a new room until that room has finished processing your last active game.");
                break;
            }
            case MAXROOMSCREATEDNOTIFY:
            {
                chatArea.append("Unable to comply, maximum room count reached.");
                break;
            }
            case INVALIDROOMPSWNOTIFY:
            {
                chatArea.append( "Password was invalid, please try again.");
                break;
            }
            case INVALIDROOMNAMELENGTHNOTIFY:
            {
                chatArea.append("Room names have to be within "+ MIN_NAMELENGTH + " and " + MAX_NAMELENGTH + " characters long. Please try again.");
                break;
            }
            case INVALIDROOMPSWLENGTHNOTIFY:
            {
                chatArea.append("Password length is illegal. Please make sure the password is between "+MIN_PASSWORDSIZE + " and " + MAX_PASSWORDSIZE
                         + " characters long.");
                break;
            }
            case PSWPROTECTEDNOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                int gameid = ref.getValue1();
                
                //First we need to get the gameName, used by the dialog.
                DefaultMutableTreeNode node = findNode(gameid,ROOMNODE);
                roomData data = (roomData)node.getUserObject();
                
                //prompt the user to enter a password, and send a new join request, thid time with a password
                System.out.println("gameid: "+gameid+"\tname: "+data.getRoomName() );
                Window registerWindow = SwingUtilities.getWindowAncestor(this);
                enterPasswordDialog dialog = new enterPasswordDialog(registerWindow,platform,gameid,data.getRoomName() );
                break;
            }
            case INVALIDROOMNAMEEXISTSNOTIFY:
            {
                if ( !(container instanceof dataContainer) )
                    throw new IllegalStateException("container supplied is not of type dataContainer");
                
                chatArea.append("That name has already been taken by another room, please try again.");
                
                break;
            }
            case CHAT:
            {
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                
                int fd = ref.getValue2();
                int[] textRaw = ref.getData();
                    
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < textRaw.length; i++)
                    sb.append( (char)textRaw[i] );
                
                String text = sb.toString();

                //we'll have to find the correct user node first since we want to display who spoke.
                DefaultMutableTreeNode user = findNode(fd,USERNODE );
                userData data = (userData) user.getUserObject();
                
                chatArea.append(data.getUserAlias() + ": " + text);
                break;
            }
            default:
                System.out.println("error: Opcode: " + container.getOpcode() + "Is unknown");
                break;
        }
    }
    
    private void onSortRooms(boolean isAscending)
    {
        roomRootNode = sortTreeNode(roomRootNode,isAscending);
        //testSort(roomRootNode,isAscending);
        roomModel.reload();
    }
    
    private void onSortLoungers(boolean isAscending)
    {
        lobbyerRootNode = sortTreeNode(lobbyerRootNode,isAscending);
        lobbyerModel.reload();
    }
    
    private void onJoin()
    {
        //check if selectedNode is actually set, or else nullpointerException will happen.
        //This can happen if the user doesn't select anything in the model before attempting to join.
        if ( selectedNode == null)
        {
            chatArea.append("Please select a room.");
            return;
        }
        
        //check if selectedNode is indeed a room node, and if so send a join request.
        Object data = selectedNode.getUserObject();
        if ( data instanceof roomData)
        {
            int roomid = ((roomData) data).getRoomId();
            platform.getExecutor().execute(new GenericCommandRunner(platform.getNetworker(),JOIN,roomid,INTEGERFALSE ) );
        }
        else//the user tried to join nothing, or a user node.
        {
            chatArea.append("Please select a room.");
        }
    }
    
    private void onCreate()
    {
        Window registerWindow = SwingUtilities.getWindowAncestor(this);
        createDialog dialog = new createDialog(registerWindow, platform);
    }
    
    private void onInput()
    {
        //Simple enough, get the data, empty the chatInput display area, send it asyncronously to the networker. lastly we make sure foucs returns
        //to the lobbyGUI itself.
        String input = chatInput.getText();
        chatInput.setText("");
        if (input.length() != 0)
        {
            platform.getExecutor().execute(new ChatMessageToSocketRunner(platform.getNetworker(),input) );
        }
        
        requestFocusInWindow();
    }
    
    private void onNameChange()
    {
        String newname = JOptionPane.showInputDialog(this,"Please enter your new username: ","Change your nick?",JOptionPane.QUESTION_MESSAGE );
        
        if (newname != null && !newname.isEmpty() )
            platform.getExecutor().execute(new WriteDisplayNameChangeRequestToSocketRunner(platform.getNetworker(),newname) );
    }
    
    private void onLogout()
    {
        platform.getExecutor().execute(new DisconnectFromServerRunner(platform.getNetworker()));
    }
    
    //useful helper, returns a reference to a node specified by the two arguments. 
    //The identifier either means the roomid or serversidefd depending on node type, 
    //which is the second argument.
    private DefaultMutableTreeNode findNode(int identifier,boolean nodeType)
    {
        Enumeration<DefaultMutableTreeNode> roomTreeNodes = roomRootNode.depthFirstEnumeration();
        Enumeration<DefaultMutableTreeNode> lobbyerTreeNodes = lobbyerRootNode.depthFirstEnumeration();
        
        //first loop goes through the room nodes, or "left tree" if you wish.
        DefaultMutableTreeNode node = searchEnumerationForNode( roomTreeNodes,identifier,nodeType );
        
        //If node is null, we need to check the other tree
        if ( node != null )
            return node;
        
        node = searchEnumerationForNode( lobbyerTreeNodes,identifier,nodeType );
        
        //second try with second tree, if this one is also null, something is off.
        if ( node != null )
            return node;
        else
        {
           //If we get this far, then the targetNode doesn't exist in either tree. That's problematic.
            if (nodeType == ROOMNODE)
                throw new IllegalStateException("Fatal error: Room Node with roomid: " + identifier + " does not exist.");
            else
                throw new IllegalStateException("Fatal error: User node with serversidefd: "+ identifier + " does not exist."); 
        }  
        
    }
    
    //looks through a iteration, and if it finds the node in question, returns a pointer to it. If not null
    private DefaultMutableTreeNode searchEnumerationForNode( Enumeration<DefaultMutableTreeNode> enumeration,int identifier,boolean nodeType )
    {
        while ( enumeration.hasMoreElements() )
        {
            DefaultMutableTreeNode currentNodeptr = enumeration.nextElement();
            Object data = currentNodeptr.getUserObject();
            
            if (nodeType == ROOMNODE)
            {
                if (data instanceof roomData)
                {
                    if ( ((roomData) data).getRoomId() == identifier)
                        return currentNodeptr;
                }
            }
            else
            {
                if (data instanceof userData)
                {
                    if ( ((userData)data).getServersideFd() == identifier)
                        return currentNodeptr;
                }
            }
        }
        return null;
    }
    
    //this method is intended to sort the two trees in two directions. Thanks to the oracle community for this simple algorithm.
    //Modified to cater to my usage.
    //In hindsight, I am not sure I am satisified with this algorithm, it relies on bubblesort, one of the slowest sorting algoritms.
    //The problem is theoretical. At say 20 rooms or users, it really doesn't matter. But at say 10000? Then it might become untenable.
    private DefaultMutableTreeNode sortTreeNode(DefaultMutableTreeNode rootnode,boolean isAscending)
    {
        boolean recurseNeeded = false;//noticed that this algoritm isn't perfect, it doesn't do multiple passes.
        DefaultMutableTreeNode newRoot = new DefaultMutableTreeNode();
        
        int childCount = rootnode.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)rootnode.getChildAt(i);
            String nodeString = node.getUserObject().toString();
            
            for (int j = 0; j<i; j++)
            {
                DefaultMutableTreeNode otherNode = (DefaultMutableTreeNode) rootnode.getChildAt(j);
                String prevNodeString = otherNode.getUserObject().toString();
                
                if ( isAscending )
                {
                    if ( nodeString.compareToIgnoreCase(prevNodeString) < 0 )
                    {
                        rootnode.insert(node, j);
                        rootnode.insert(otherNode, i);
                        recurseNeeded = true;
                    }
                }
                else
                {
                    if ( nodeString.compareToIgnoreCase(prevNodeString) > 0 )
                    {
                        rootnode.insert(node, j);
                        rootnode.insert(otherNode, i);
                        recurseNeeded = true;
                    }
                }
            }
            if ( node.getChildCount() > 0 )
                node = sortTreeNode(node,isAscending);
        }
        if ( recurseNeeded )
            sortTreeNode(rootnode,isAscending);
        return rootnode;
    }
    
    /*As a note, the original Qt implementation was less than ideal, it forced me to work with this arcane concept of "user roles" when storing custom 
    node data. Luckily, we won't have to do that in swing, as I we instead simply define raw data classes that the data nodes just point to upon creation.
    furthermore, the user roles I originally defined represented the following:
    user role +1, which was the clients file descriptor id on the server, used to identify the node in the view. 
    pretty clever to reuse the servers fd descriptors in this way, I dare say.
    user role +2, roomid, or roomid as I've inconsistently been calling it through the original codebase, something I need to correct. 
    user role +3, same as +2, except this index, or whatever you would call it, doubled as a means of saying that this is a room node and not a user node.
    As you see the third user role was unnecessary. A simple absence of user role +1 would've been enough to identify a node as room node.*/
    private class roomData //used in the tree to represent room nodes.
    {
        private int roomid;
        private String roomName;
        
        public roomData(int roomidref, String roomNameRef) 
        { 
            roomid = roomidref;
            roomName = roomNameRef;
        }
        
        void setRoomId(int id) { roomid = id; }
        int getRoomId() { return roomid; }
        String getRoomName() { return roomName; }
        
        @Override
        public String toString() { return roomName; }
        
    }
    
    private class userData//like room node except to represent actual players, whether they are lounging or playing.
    {
        private String userAlias;
        private int roomid;
        private int serversidefd;//like I said, this is the fd the server has for this client, it's not the local fd the client uses to communicate with the server.
        
        public userData(String userAliasref,int roomidref,int serversidefdref)
        {
            userAlias = userAliasref;
            roomid = roomidref;
            serversidefd = serversidefdref;
        }
        
        void setUserAlias(String alias ) { userAlias = alias; }
        String getUserAlias() { return userAlias; }
        
        void setRoomId(int id) { roomid = id; }
        int getRoomId() { return roomid; }
        
        void setServersideFd(int fd) { serversidefd = fd; }
        int getServersideFd() { return serversidefd; }
        
        @Override
        public String toString() { return userAlias; };
    }
}