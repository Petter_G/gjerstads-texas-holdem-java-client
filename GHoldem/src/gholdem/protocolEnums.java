package gholdem;

/*Note: 
    SERVERCOMMANDS and CLIENTENUMS both contain what I call "opcodes", codes used to trigger a certain behaviour on the other side. Each request sendt over the ether will consist of
    segments, where each segmenet is exactly four bytes. The length of a request is thus always 4 * n segments. Might strike you as somewhat inefficient, and it is. May be that I improve upon
    the efficiency of the protocol at some point.
    
    At any rate, together they make up my protocol, the means by which the server(call it the backend if you want to) and the client(s) communicate.
*/
        
public class protocolEnums
{
    
    //0-99, Commands, or more accurately requests, the clients can send to the server.
    public class SERVERCOMMANDS
    {
        public static final int SETREADY = 10;
        public static final int CLEARREADY = 2;
        public static final int CHECK = 3;
        public static final int CALL = 4;
        public static final int FOLD = 5;
        public static final int RAISE = 6;
        public static final int ALLIN = 8;
        public static final int SETNAME=7;      //opcode-characters(1-20)
        public static final int RESET=9;
        public static final int REFILL=11;       //opcode-amount //Brukes for å fylle opp konto.
        public static final int TRANSFER=12;     //opcode-amount //Ber om en overføring av penger fra escrow til konto. I praksis blir ikke overføringen fullført før neste parti.
        public static final int LOGIN = 13;       //opcode-userlength-passlength-username-password
        public static final int REGISTER = 14;    //opcode-userlength-passlength-username-password
        public static final int JOIN = 15;        //opcode-gameid-usingPSW-pswCount-psw
        public static final int CREATE = 16;         //opcode-pswprot-namecount-name-pswcount-psw //pswprot is a boolean. If true then pswcount and psw exists.
                                                                                                  //namecount and name is always present however.
        public static final int LEAVEROOM = 17;      //opcode
    }

    //100-199, Notifications coming in from the server. They tell the client to do specific things. The comments describe what each segment in the package means.
    public class CLIENTENUMS
    {
        public static final int INVALIDCOMMAND = 100; //opcode
        public static final int TABLESLOTNOTIFY = 101;//opcode-tableslot-trulyNew-local
                                                    //trulyNew is a bool that denotes if the player is new to the game, or just that particular client. 
                                                    //Used exclusively to create the proper console area notes.
                                                    //Local denotes if this notification relates to the local player.
        public static final int PLAYERSNOTREADY = 102;//opcode
        public static final int VALIDCOMMAND = 103;//opcode
        public static final int NAMECHANGEDNOTIFY = 104;//opcode-serversidefd-silent-tableslot-messagelength-character(1-20) variabel lengde //messagelength inkluderer kun meldingen selv.
                                //silent brukes til å styre hvorvidt det skal gis en "is now known as" melding i konsol vinduer.
        public static final int TABLESLOTCHANGEDNAMENOTIFY =141;//opcode-tableslot-silent-namelength-character(1-20)//brukes for å fortelle et roms pillere at en av dem har skiftet navn.
        public static final int REPAINT = 105;//opcode

        public static final int HANDUPDATE = 106;//opcode-tableslot-card1type-card1value-card2type-card2value
        public static final int DELETEREP = 107;//opcode-tableslot

        public static final int DUMPHANDUPDATE = 108;//opcode-tableslot
        public static final int BETUPDATE = 109;//opcode-tableslot-bet-totalbet
        public static final int BALANCEUPDATE = 110;//opcode-tableslot-balance
        public static final int ACTIVEUPDATE = 111;//opcode-tableslot-activestate
        public static final int UPDATEREADY = 112;//opcode-tableslot-activestate
        public static final int ALLINUPDATE = 123;//opcode-tableslot    //kalles aldri for å gjøre en allin tilstand false, ved communityreset skjer dette automatisk.

        public static final int NAMETOLONGNOTIFY = 113;//opcode
        public static final int TABLERESET = 114;//opcode
        public static final int COMMUNITYCARDADDED = 115;//opcode-card1type-card1value

        public static final int POTNOTIFY = 116; //opcode-potvalue
        public static final int CURRENTPLAYERNOTIFY = 117; //opcode-currentplayer
        public static final int CURRENTBUTTONNOTIFY = 118; //opcode-currentButton

        public static final int MINIMUMRAISEFAILURENOTIFY = 119; //opcode-bigblind
        public static final int PAUPERFAILURENOTIFY = 120; //opcode
        public static final int NOTYOURTURNNOTIFY = 121; //opcode
        public static final int CHECKINVALIDNOTIFY = 122; //opcode
        public static final int REJECTCONNECTIONNOTIFY = 124;//opcode
        public static final int GAMEHALTEDNOTIFY = 125;  //opcode
        public static final int GAMEPAUSEDNOTIFY = 128; //opcode

        public static final int WINNERINFONOTIFY = 126;  //opcode-winnertableslot-payout-potIndex
        public static final int JOINSUCCESSNOTIFY = 127; //opcode
        public static final int JOINFAILURENOTIFY = 142; //opcode
        //public static final int AUTHENTICATEREQUIREDNOTIFY = 129; //opcode //obsolete
        public static final int NAMECHANGEINVALIDNOTIFY = 130; //opcode
        public static final int AUTHENTICATIONFAILURENOTIFY = 131; //opcode
        public static final int AUTHENTICATIONSUCCESSNOTIFY = 132; //opcode-cookieid

        public static final int INVALIDPROFILECREATION = 133;    //opcode
        public static final int VALIDPROFILECREATION = 134;       //opcode
        public static final int SERVERSHUTDOWNNOTIFY = 135;         //opcode
        public static final int NEWGAMENOTIFY = 136;             //opcode-gameid-roomnameCount-roomname
        public static final int NEWPLAYERNOTIFY = 137;           //opcode-gameid-serversidefd-messagelength-character(1-20)
                                        //serverside fd brukes som ekstra data, slik at det er lettere å finne objektet hos klientene
                                        //fd for hver enkelt klient endrer seg ikke, og den gjenbrukes ikke før klienten er helt utlogget uansett
        public static final int CLIENTDISSCONNECTEDNOTIFY = 138;           //opcode-serversidefd
        public static final int PLAYERJOINEDROOMNOTIFY = 139;      //opcode-serversidefd-gameid
        public static final int PLAYERLEFTLOCALROOMNOTIFY = 140;      //opcode-tableslot//brukes når spiller trekker seg fra et rom.
        public static final int PLAYERSTATECHANGEDNOTIFY = 143;  //opcode-serversidefd-gameid-dcstate //gameid = -1 player was a lounger, ie
        public static final int MAXROOMSCREATEDNOTIFY = 144;    //opcode
        
        public static final int INVALIDROOMNAMELENGTHNOTIFY = 155;             //opcode
        public static final int INVALIDROOMPSWLENGTHNOTIFY = 156;              //opcode
        public static final int INVALIDROOMNAMEEXISTSNOTIFY = 159;          //opcode
        public static final int INVALIDROOMPSWNOTIFY = 157;              //opcode   //NOTE: signals that the password supplied when trying to join is wrong
        public static final int PSWPROTECTEDNOTIFY = 158;                //opcode-gameid    //NOTE: Signals that the room the player is trying to join is password protected, IE try again with password.
    }

    //200-255 unused.
    public class MessageEnums
    {
        public static final int SHORTCHAT  = 201; //opcode-character(1-2000)//variant brukt ved kommunikasjon sendt fra klient til server. Implementert for letthetens skyld
        public static final int CHAT = 200; //opcode-tableslot-fd-charcount-character(1-2000) variabel lengde //-1 betyr lounger
    }

    public class cardValuesEnums
    {
        public static final int ACELOW = 1;
        public static final int TWO = 2;
        public static final int THREE=3;
        public static final int FOUR=4;
        public static final int FIVE=5;
        public static final int SIX=6;
        public static final int SEVEN=7;
        public static final int EIGHT=8;
        public static final int NINE=9;
        public static final int TEN=10;
        public static final int JACK=11;
        public static final int QUEEN=12;
        public static final int KING=13;
        public static final int ACEHIGH = 14;
    }

    public class cardTypesEnums
    {
        public static final int CLUBS=0;
        public static final int DIAMONDS=1;
        public static final int HEARTS=2;
        public static final int SPADES=3;
    }

    public class scoreValuesEnums
    {
        public static final int ROYALFLUSH=10;
        public static final int STRAIGHTFLUSH=9;
        public static final int FOUROFAKIND=8;
        public static final int HOUSE=7;
        public static final int FLUSH=6;
        public static final int STRAIGHT=5;
        public static final int THREEOFAKIND=4;
        public static final int TWOPAIR=3;
        public static final int PAIR=2;
        public static final int HIGHCARD=1;
    }

    public class BlockEuphemisms
    {
        public static final int ONE_BLOCK = 1;
        public static final int TWO_BLOCKS = 2;
        public static final int THREE_BLOCKS = 3;
        public static final int FOUR_BLOCKS = 4;
        public static final int FIVE_BLOCKS = 5;
        public static final int SIX_BLOCKS = 6;
    }

    //Various other euphemisms
    public class DIV
    {
        public static final String CONFIGFILEPATH = "./HoldemConfig.cfg";//Should result in the file being located in the same directory as the executable
        public static final int DEFAULTPORT = 3500;
        public static final String RESOURCEBASE = "resources/images/";//original form: QString
        static final float CARDOPACITY_ON = 0.5f;
        static final float CARDOPACITY_OFF = 1f;
        public static final boolean LEFT = false;
        public static final boolean RIGHT = true;
        
        public static final boolean ROOMNODE = true;
        public static final boolean USERNODE = false;
        public static final boolean LOGINATTEMPT = true;
        public static final boolean REGISTERATTEMPT = false;
        
        public static final int CONNECT_TIMEOUT = 5000;
        
        public static final int MAX_MESSAGESIZE=2000;
        public static final int MIN_NAMELENGTH = 3;
        public static final int MIN_PASSWORDSIZE = 3;
        public static final int MAX_PASSWORDSIZE = 20;
        public static final int MAX_NAMELENGTH=20;
        public static final int MAX_PLAYERS = 8;
        public static final int OPCODE_SIZE = 4;
        public static final int SEGMENT_SIZE = 4;

        public static final boolean UPPER_PLAYER_ANIMATION = true;
        public static final boolean LOWER_PLAYER_ANIMATION = false;
        
        public static final int INTEGERTRUE = 1;
        public static final int INTEGERFALSE = 0;
        
        
    }
}