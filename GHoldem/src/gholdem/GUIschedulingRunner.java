package gholdem;
/*
A "runner" refers to classes that implements the Runnable interface.
Any use of the java swing framework will involve the automatic creation of the "event dispatch thread", or EDT for short.
Similarily the java.util.concurrent package defines the executors, neat implementations of various concurrency related ideas. 
Both the swing framework and the concurrency framework require you to implement your own subclass of the Runnable interface.
These subtypes can then include data you want to share, as well as what you actually want the task to do in the form of overriding the run() method.

In Qt I could rely on the signals and slots mechanism to send data safely across threads, here I will have to do so myself.
If you've studied the original implementation, you will recognize that the names of these runners are the same as the signals used 
to communicate with the networker there.
*/

//The following runners are exclusively invoked indirectly from the networker. 
//These runners enables the networker to schedule GUI updates on the EDT. 
abstract class BaseGUISchedulingRunner implements Runnable
{
    protected platform platform;
    
    public BaseGUISchedulingRunner(platform platformref)
    {
        platform = platformref;
    }
    
    @Override
    abstract public void run();
}

class GUISchedulingRunner extends BaseGUISchedulingRunner
{
    dataContainer container;
    
    public GUISchedulingRunner(platform platform,dataContainer container)
    {
        super(platform);
        this.container = container;
    }

    @Override
    public void run()
    {
        platform.processInvocation(container);
    }
    
}

//this runner basically schedules the 
class SignalGUI_resetYourselfRunner extends BaseGUISchedulingRunner
{
    public SignalGUI_resetYourselfRunner(platform platformref)
    {
        super(platformref);
    }

    @Override
    public void run()
    {
        platform.resetGUI();
    }

}