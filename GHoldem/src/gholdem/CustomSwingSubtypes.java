package gholdem;

import static gholdem.protocolEnums.SERVERCOMMANDS.*;
import static gholdem.protocolEnums.DIV.*;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

//works just like the regular version, except it has some values set already.
class PersonalizedJToggleButton extends JToggleButton 
{

    ImageIcon sortedSelectedIcon;
    ImageIcon sortedDeSelectedIcon;

    public PersonalizedJToggleButton() 
    {
        sortedSelectedIcon = new ImageIcon(getClass().getClassLoader().getResource(RESOURCEBASE + "ascending.png"));
        sortedDeSelectedIcon = new ImageIcon(getClass().getClassLoader().getResource(RESOURCEBASE + "descending.png"));

        this.setIcon(sortedDeSelectedIcon);
        this.setSelectedIcon(sortedSelectedIcon);
        this.setBorderPainted(false);
        this.setContentAreaFilled(false);
        this.setFocusPainted(false);
        this.setOpaque(false);
    }
}

//Identical to the regular version except this one adds the current time and endline character to the appended text.
class AutoDatingJTextArea extends JTextArea 
{

    @Override
    public void append(String text) 
    {
        //little hack to compensate for blank strings resulting in just a timestamp and nothing else.
        //The basic implementation doesn't have this problem because invocing doc.insertString with a empty string 
        //results in no action. The formatted string that I create here, always has content(timestamp and colon, 
        //and thus length is never zero.
        if (text.length() == 0) 
        {
            return;
        }
        Document doc = getDocument();
        if (doc != null) 
        {
            try 
            {
                LocalTime time = LocalTime.now();
                time = time.truncatedTo(ChronoUnit.SECONDS);
                String formatted = time.toString();
                formatted += ": " + text + "\n";

                doc.insertString(doc.getLength(), formatted, null);
            } catch (BadLocationException e) //I am just copying the default implementation here.
            {
            }
        }
    }
}

//identical to regular JLabel, except this one stores a blank icon for use when the item is invisible via setVisible(false)
//upon creation the blank icon is used. If one wants to show something else, you have to explicitely give it another icon through setIcon, and then
//call setVisible(true) in order to actually show the icon.
class ConvenientJLabel extends JLabel 
{

    boolean visible;

    Icon blankIcon;
    Icon cardIcon;

    ConvenientJLabel() 
    {
        super();
        visible = false;

        String blankURL = RESOURCEBASE + "blank.png";
        blankIcon = new ImageIcon(getClass().getClassLoader().getResource(blankURL));

        super.setIcon(blankIcon);//blank by default.
    }

    //sets icon visibile or invisible depending on state
    @Override
    public void setVisible(boolean state) 
    {
        if (state == true)//switch to visible side
        {
            visible = true;
            super.setIcon(cardIcon);
        } else 
        {
            visible = false;
            super.setIcon(blankIcon);
        }
    }

    //differs from the regular version in that it doesn't actually display the icon, merely stores it for later use.
    @Override
    public void setIcon(Icon icon) 
    {
        cardIcon = icon;
    }

    public boolean getVisible() 
    {
        return visible;
    }

}

//Used in the handWidget to represent the cards. Identical to the regular JLabels except these ones can have their opacity turned on or off as needed.
class opacityEnabledJLabel extends JLabel 
{
    //thanks to http://stackoverflow.com/questions/18042542/change-jbutton-transparancy-opacity-alpha for the solution
    //Slightly modified for my use.
    private float alpha = 1f;

    public void setAlpha(float alpha) 
    {
        this.alpha = alpha;
        repaint();
    }

    @Override
    public void paint(Graphics g) 
    {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
        super.paint(g2);
        g2.dispose();
    }
};

//This styled JLabel automatically renders its text in white, with a rounded blue rectangle around. 
class StyledJLabel extends JLabel 
{

    Color componentBackground;
    Dimension arcs;

    //Big thanks to BackSlash over on StackOverflow.com who came up with the solution for someone else.
    //Slightly modified for my own use.
    StyledJLabel() 
    {
        super();
        componentBackground = Color.GRAY;
        setFont(new Font("Verdana", Font.BOLD, 18));
        setForeground(Color.white);
        arcs = new Dimension(20, 20);
    }

    @Override
    protected void paintComponent(Graphics g) 
    {
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //Draws the rounded opaque panel with borders.
        graphics.setColor(getBackground());
        graphics.setPaint(new GradientPaint(0, 0, componentBackground.darker(), 0, getHeight(), componentBackground.brighter()));
        graphics.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, arcs.width, arcs.height);//paint background
        graphics.setColor(this.getParent().getBackground());
        graphics.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, arcs.width, arcs.height);//paint border*/

        super.paintComponent(g);
    }
}

class enterPasswordDialog extends JDialog
{
    JLabel passwordLabel;
    JPasswordField passwordField;
    
    JButton ok;
    JButton cancel;
    
    platform platform;
    
    enterPasswordDialog(Window frame, platform platformRef,int gameid,String roomName)
    {
        super(frame,"Enter Password",Dialog.ModalityType.DOCUMENT_MODAL);
        platform = platformRef;
        
        passwordLabel = new JLabel("Password: ");
        passwordLabel.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        
        passwordField = new JPasswordField();
        
        ok = new JButton("Ok");
        cancel = new JButton("Cancel");
        
        //Action listeners
        ok.addActionListener((ActionEvent e) ->
        {
            String psw = String.valueOf(passwordField.getPassword() );
            
            GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), JOIN, gameid,INTEGERTRUE,psw.length(),psw);
            
            platform.getExecutor().execute(runner);
            dispose();
        });
        cancel.addActionListener((ActionEvent e) -> 
        {
            dispose();
        });
        
        //keybindings
        getRootPane().registerKeyboardAction((ActionEvent e) -> 
        {
            cancel.doClick();
        },KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        
        getRootPane().registerKeyboardAction((ActionEvent e) -> 
        {
            ok.doClick();
        },KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        
        //Layouting
        JLabel help = new JLabel("Please supply a password in order to join \""+roomName + "\".");
        help.setBorder(BorderFactory.createEmptyBorder( 0,0, 10, 0));
        
        JPanel fieldsPanel = new JPanel(new GridLayout(1,2));

        fieldsPanel.add(passwordLabel);
        fieldsPanel.add(passwordField);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout( new GridBagLayout() );
        
        buttonPanel.add(ok);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(cancel);
        
        JPanel masterPanel = new JPanel();
        masterPanel.setLayout(new GridBagLayout() );
        masterPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        
        GridBagConstraints cons = new GridBagConstraints();
        cons.fill = GridBagConstraints.BOTH;
        cons.gridx = GridBagConstraints.RELATIVE;
        cons.gridy = 0;
        
        masterPanel.add(help,cons);
        cons.fill = GridBagConstraints.BOTH;
        cons.gridy = 1;
        masterPanel.add(fieldsPanel,cons);
        cons.gridy = 2;
        masterPanel.add(Box.createVerticalStrut(5),cons);
        cons.gridy = 3;
        masterPanel.add(buttonPanel,cons);
        this.setContentPane(masterPanel);
        
        setResizable(false);
        pack();
        setLocationRelativeTo(frame);
        setModal(true);
        setVisible(true);
    }
}

//create dialog, enables the user to enter a username and optional password
class createDialog extends JDialog 
{

    JLabel roomNameLabel;
    JTextField roomNameField;
    JLabel passwordLabel;
    JPasswordField passwordField;
    JButton ok;
    JButton cancel;

    platform platform;

    createDialog(Window frame, platform platformRef) 
    {
        super(frame, "Enter a name for the new room.", Dialog.ModalityType.DOCUMENT_MODAL);
        platform = platformRef;

        roomNameLabel = new JLabel("Room name: ");
        roomNameLabel.setAlignmentX(LEFT_ALIGNMENT);
        passwordLabel = new JLabel("Password: (Optional) ");
        passwordLabel.setAlignmentX(LEFT_ALIGNMENT);

        roomNameField = new JTextField();
        passwordField = new JPasswordField();

        ok = new JButton("Ok");
        cancel = new JButton("Cancel");

        //listeners
        ok.addActionListener((ActionEvent e) -> 
        {
            System.out.println("test");
            GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), CREATE,
                    roomNameField.getText(),String.valueOf(passwordField.getPassword() ) );
            
            platform.getExecutor().execute(runner);
            dispose();
        });
        cancel.addActionListener((ActionEvent e) -> 
        {
            dispose();
        });
        
        //keybindings
        getRootPane().registerKeyboardAction((ActionEvent e) -> 
        {
            cancel.doClick();
        },KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        
        getRootPane().registerKeyboardAction((ActionEvent e) -> 
        {
            ok.doClick();
        },KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        //Layouting
        
        JPanel fieldsPanel = new JPanel(new GridLayout(2,2));
        fieldsPanel.add(roomNameLabel);
        fieldsPanel.add(roomNameField);
        
        fieldsPanel.add(passwordLabel);
        fieldsPanel.add(passwordField);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout( new GridBagLayout() );
        
        buttonPanel.add(ok);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(cancel);
        
        JPanel masterPanel = new JPanel();
        masterPanel.setLayout(new GridBagLayout() );
       
        GridBagConstraints cons = new GridBagConstraints();
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.gridx = GridBagConstraints.RELATIVE;
        cons.gridy = 0;
        masterPanel.add(fieldsPanel,cons);
        
        cons.gridy = 1;
        masterPanel.add(Box.createVerticalStrut(5),cons );
        cons.gridy = 2;
        masterPanel.add(buttonPanel,cons);
        masterPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        
        this.setContentPane(masterPanel);
        
        setResizable(false);
        pack();
        setLocationRelativeTo(frame);
        setModal(true);
        setVisible(true);
    }
}

//Custom dialog, somewhat generalized
class spinnerQuestionDialog extends JDialog 
{

    JLabel question;
    JSpinner inputSpinner;
    JButton cancel;
    JButton ok;

    platform platform;

    spinnerQuestionDialog(Window frame, platform platformRef, String questionRef, String title, int increment, int commandopcode) 
    {
        super(frame, title, Dialog.ModalityType.DOCUMENT_MODAL);
        platform = platformRef;

        //initialize the various fields
        question = new JLabel(questionRef);

        SpinnerNumberModel model = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, increment);
        inputSpinner = new JSpinner(model);
        ((JSpinner.DefaultEditor) inputSpinner.getEditor()).getTextField().setEditable(false);

        cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                dispose();
            }
        });
        ok = new JButton("Ok");
        ok.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                //the server takes care of inputvalidation.
                switch (commandopcode) 
                {
                    case RAISE: 
                    {
                        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), commandopcode,
                                model.getNumber().intValue());
                        platform.getExecutor().execute(runner);
                        break;
                    }
                    case REFILL: 
                    {
                        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), commandopcode,
                                model.getNumber().intValue());
                        platform.getExecutor().execute(runner);
                        break;
                    }
                    case TRANSFER: 
                    {
                        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), commandopcode,
                                model.getNumber().intValue());
                        platform.getExecutor().execute(runner);
                        break;
                    }
                    default: 
                    {
                        System.out.println("Error in spinnerQuestionDialog, opcode unknown.");
                    }
                }
                dispose();
            }

        });

        //Mnemonics, we must access the spinners editor, which is little more than a text field with seemingly forced focus.
        ((JSpinner.DefaultEditor) inputSpinner.getEditor()).getTextField().addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyPressed(KeyEvent e) 
            {
                switch (e.getKeyCode()) 
                {
                    case KeyEvent.VK_ESCAPE:
                        cancel.doClick();
                        break;
                    case KeyEvent.VK_ENTER:
                        ok.doClick();
                        break;
                    default:
                        System.out.println("Key pressed: " + e.getKeyCode());
                }
            }
        });

        //layouting
        setLayout(new GridBagLayout());

        GridBagConstraints constraint = new GridBagConstraints();
        GridBagConstraints helper = new GridBagConstraints();
        GridBagConstraints helper2 = new GridBagConstraints();
        constraint.gridy = 0;
        constraint.gridx = 1;
        constraint.insets = new Insets(5, 5, 5, 5);
        helper.gridx = 0;
        helper2.gridx = 2;

        add(new JPanel(), helper);
        add(question, constraint);
        add(new JPanel(), helper2);

        constraint.gridy = 1;
        constraint.fill = GridBagConstraints.HORIZONTAL;
        add(inputSpinner, constraint);

        //I tried to implement the same look without using this wrapper panel, I was unsuccessful. This way is just way faster and easier to implement
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));

        constraint = new GridBagConstraints();
        constraint.gridy = 2;
        constraint.gridx = 1;
        constraint.insets = new Insets(5, 5, 5, 5);

        buttonPanel.add(ok);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(cancel);
        add(buttonPanel, constraint);

        //some final configuration
        setResizable(false);
        pack();
        setLocationRelativeTo(frame);
        setModal(true);
        setVisible(true);
    }
}
