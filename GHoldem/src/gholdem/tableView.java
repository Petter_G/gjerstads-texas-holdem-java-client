package gholdem;

import static gholdem.protocolEnums.DIV.*;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/*originally implemented as the SetupIcon function. The function merely manipulated fields beheld directly by the GUI with the exception of the 
handWidget class which was defined elsewhere.*/
public class tableView extends JPanel
{

    int horizontalOffset = 0;
    int upperSlotLimit = MAX_PLAYERS / 2;

    //originally the holdemGUI itself was the owner of the players array. This time that responsibility has been delegated to the tableView.
    handWidget[] players;
    ConvenientJLabel[] communitySlots;
    StyledJLabel potField;

    public tableView()
    {
        players = new handWidget[MAX_PLAYERS];
        communitySlots = new ConvenientJLabel[5];

        JPanel UVP = new JPanel();//UVP= UpperVisiblePanel, originally UVL(upperVisibilePanel)
        JPanel MVP = new JPanel();//MVP = MiddleVisibilePanel
        JPanel LVP = new JPanel();//LVP = LowerVisibilePanel

        //fill up the slots with default handWidgets, starting with the upper ones.
        for (int i = 0; i < upperSlotLimit; i++)
        {
            players[i] = new handWidget(UPPER_PLAYER_ANIMATION);
            UVP.add(players[i]);
        }
        //Now the lower handwidgets.
        for (int i = upperSlotLimit; i < MAX_PLAYERS; i++)
        {
            players[i] = new handWidget(LOWER_PLAYER_ANIMATION);
            LVP.add(players[i]);
            horizontalOffset++;
        }

        //and now for the community slots, placed in the MVP.
        MVP.add(Box.createHorizontalStrut(15));
        for (int i = 0; i < 5; i++)
        {
            communitySlots[i] = new ConvenientJLabel();

            MVP.add(communitySlots[i]);
            //we want some spcaing between the components as well
            MVP.add(Box.createHorizontalStrut(20));
        }

        //We also need to show the pot.
        potField = new StyledJLabel();
        potField.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        setPot(0);

        //We need a wrapper panel to show the potField the way we want
        JPanel potFieldWrapper = new JPanel();
        potFieldWrapper.setLayout(new BorderLayout());

        potFieldWrapper.add(Box.createVerticalStrut(25), BorderLayout.NORTH);
        potFieldWrapper.add(potField, BorderLayout.CENTER);
        potFieldWrapper.add(Box.createVerticalStrut(25), BorderLayout.SOUTH);
        potFieldWrapper.add(Box.createHorizontalStrut(20), BorderLayout.WEST);

        UVP.setLayout(new BoxLayout(UVP, BoxLayout.X_AXIS));
        LVP.setLayout(new BoxLayout(LVP, BoxLayout.X_AXIS));
        MVP.setLayout(new BoxLayout(MVP, BoxLayout.X_AXIS));
        MVP.setOpaque(true);
        MVP.add(potFieldWrapper);
        MVP.add(Box.createHorizontalStrut(20));

        //setup the layout for the local contentpane.
        UVP.setLayout(new BoxLayout(UVP, BoxLayout.X_AXIS));
        LVP.setLayout(new BoxLayout(LVP, BoxLayout.X_AXIS));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        //having a background image was surprisingly difficult in swing, in Qt it was a breeze.
        //solving it means using JLayeredPane, which allows us to layer panels atop of each other.
        //which may be ironic, as I am contemplating replacing it altogether with a header above the view displaying the logo.
        GridBagConstraints constraint = new GridBagConstraints();
        JPanel viewAreaWrapper = new JPanel(new GridBagLayout());
        constraint.gridx = 0;
        constraint.gridy = 0;

        //Mash it all together.
        viewAreaWrapper.add(UVP, constraint);
        constraint.gridy = 1;
        constraint.fill = GridBagConstraints.HORIZONTAL;
        viewAreaWrapper.add(MVP, constraint);
        constraint.gridy = 2;
        viewAreaWrapper.add(LVP, constraint);

        constraint = new GridBagConstraints();
        constraint.gridx = 0;
        constraint.gridy = 0;

        add(viewAreaWrapper);
    }

    //like the original, creates URL based on the data and returns it. Note, is it worth it? the original one was only ever used twice.
    public String createCardURL(int suit, int value)
    {
        String imageURL = RESOURCEBASE + suit;

        if (value < 10)
        {
            imageURL += 0;
        }

        imageURL += value;

        return imageURL += ".gif";
    }

    public void appendCommunity(int value1, int value2)
    {
        int suit = value1;
        int cardvalue = value2;

        String imageURL = createCardURL(suit, cardvalue);

        //iterate through the communitycards, looking for the first available slot
        for (ConvenientJLabel slot : communitySlots)
        {
            if (slot.getVisible() == false)//ie blank
            {
                //set icon, then show it, then return
                ImageIcon icon = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
                slot.setIcon(icon);
                slot.setVisible(true);
                return;
            }
        }
    }

    //resets all five community cards to their defaults, ie blank.
    public void clearCommunity()
    {
        for (ConvenientJLabel slot : communitySlots)
        {
            slot.setVisible(false);
        }
    }

    public void setPot(int value)
    {
        String text = "In pot: $" + value;
        potField.setText(text);
    }

    //one might argue that this brute force "get entire array" isn't object oriented enough, may change it later. 
    public handWidget[] getPlayers()
    {
        return players;
    }

    public ConvenientJLabel[] getCommunitySlots()
    {
        return communitySlots;
    }
}
