package gholdem;

import static gholdem.protocolEnums.DIV.*;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.InetSocketAddress;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class loginGUI extends JPanel
{ 
    private JTextField usernameField;
    private JPasswordField passwordField;
    final private JButton login;
    final private JButton register;
    final private JButton options;
    registerDialogGUI registerDialog;
    Window registerWindow;
    platform platform;
        
    public loginGUI(platform parent)
    {
        platform = parent;
        
        JLabel logo = new JLabel("Welcome to Gjerstads Texas Holdem Poker");
        Font newFont = new Font("arial",Font.ITALIC | Font.BOLD, 16);
        logo.setFont(newFont );
        
        //Usernames and password fields. As well as the defining their actions when clicked.
        usernameField = new JTextField(10);
        JLabel usernameLabel = new JLabel("Username: ",SwingConstants.LEFT);
        passwordField = new JPasswordField(10);
        JLabel passwordLabel = new JLabel("Password: ",SwingConstants.LEFT);
        
        login = new JButton("Login");
        login.addActionListener((ActionEvent e) -> 
        {
            onLogin();
        });
        register = new JButton("Register new profile");
        register.addActionListener((ActionEvent e) ->
        {
            registerWindow = SwingUtilities.getWindowAncestor(register);
            registerDialog = new registerDialogGUI(registerWindow,platform);
        });
        options = new JButton("Options");
        options.addActionListener((ActionEvent e) ->
        {
            onOptions();
        });
        
        //key listenerers
        usernameField.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )//Enter will simply switch focus to the password field.
                    passwordField.requestFocusInWindow();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        passwordField.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )//unlike usernamefield, a enter here will result in a login attempt.
                   onLogin();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        login.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )//also login attempt.
                    onLogin();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        register.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )//will result in a registerDialog to show.
                    register.doClick();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        options.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if ( e.getKeyCode() == KeyEvent.VK_ENTER )//will open the option menu
                    options.doClick();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        
        //setup the layout.
        JPanel usernamePane = new JPanel();
        JPanel passwordPane = new JPanel();
        
        usernamePane.add(usernameLabel);
        usernamePane.add(Box.createHorizontalStrut(10));
        usernamePane.add(usernameField);
        
        passwordPane.add(passwordLabel);
        passwordPane.add(Box.createHorizontalStrut(10));
        passwordPane.add(passwordField);
        
        JPanel fieldsMasterPane = new JPanel();
        fieldsMasterPane.setLayout(new BorderLayout() );
        fieldsMasterPane.add(usernamePane,BorderLayout.NORTH);
        fieldsMasterPane.add(passwordPane,BorderLayout.CENTER);
        
        JPanel buttonMasterPane = new JPanel();
        JPanel buttonComponentsPane = new JPanel();
        
        buttonComponentsPane.setLayout(new BorderLayout() );
        buttonComponentsPane.add(login,BorderLayout.NORTH);
        buttonComponentsPane.add(register,BorderLayout.CENTER);
        buttonComponentsPane.add(options,BorderLayout.SOUTH);
        
        buttonMasterPane.setLayout(new BorderLayout() );
        buttonMasterPane.add(Box.createHorizontalStrut(30),BorderLayout.WEST );
        buttonMasterPane.add(buttonComponentsPane,BorderLayout.CENTER);
        buttonMasterPane.add(Box.createHorizontalStrut(30),BorderLayout.EAST);
        
        
        //BorderPane exists so I can add a nice little round border around them, just like the original implementation.
        JPanel borderPane = new JPanel()
        {
            //Big thanks to BackSlash over on StackOverflow.com who came up with the solution for someone else.
            //Slightly modified for my own use.
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                {
                    super.paintComponent(g);
                    Dimension arcs = new Dimension(30, 30);
                    int width = getWidth();
                    int height = getHeight();
                    Graphics2D graphics = (Graphics2D) g;
                    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                    //Draws the rounded opaque panel with borders.
                    graphics.setColor(getBackground());
                    graphics.fillRoundRect(0, 0, width - 1, height - 1, arcs.width, arcs.height);//paint background
                    graphics.setColor(getForeground());
                    graphics.drawRoundRect(0, 0, width - 1, height - 1, arcs.width, arcs.height);//paint border
                }
            }
        };
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(2,1));
        contentPane.add(fieldsMasterPane);
        contentPane.add(buttonMasterPane);
        contentPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        
        borderPane.add(contentPane);
        
        JPanel masterPane = new JPanel();
        masterPane.setLayout(new GridBagLayout() );
        GridBagConstraints borderConstraints = new GridBagConstraints();
        borderConstraints.gridy = 2;
        borderConstraints.fill = GridBagConstraints.HORIZONTAL;
        borderConstraints.insets = new Insets(10,10,10,10);
        GridBagConstraints logoConstraints = new GridBagConstraints();
        logoConstraints.insets = new Insets(10,10,10,10);
        
        masterPane.add(logo,logoConstraints);
        masterPane.add(borderPane,borderConstraints);
      
        add(masterPane);
        takeFocus();
    }   
    
    public void takeFocus()
    {
        usernameField.requestFocusInWindow();
    }
    
    public void onLogin()
    {
        //if the user pressed the login button, capture the two fields and do schedule a login attempt.
        String username = usernameField.getText();
        String password = String.valueOf(passwordField.getPassword());
        usernameField.setText("");
        passwordField.setText("");

        Networker networker = platform.getNetworker();
        DoProfileRunner runner = new DoProfileRunner(networker, username, password, LOGINATTEMPT);

        platform.getExecutor().execute(runner);
    }
    
    private void onOptions()
    {
        //open the dialog, fetch updated connection information, update the platform with said data.
        Window Window = SwingUtilities.getWindowAncestor(register);
        optionsDialog dialog = new optionsDialog( Window,platform,
            ConfigHandler.getIPTarget(),
            ConfigHandler.getPortTarget() );
        InetSocketAddress result = dialog.showDialog();
        
        if (result != null)
            ConfigHandler.writeConfigToFile(result.getHostName(),result.getPort() );
    }
    
    public void closeRegisterDialog()
    {
        //something really ugly is going on behind the scenes here.
        //For some bizare reason, the registerDialog reference is nulled at some point in the API. this ofcourse explains the nullpointer exceptions.
        //closing the GUI manually works just fine, indicating that the object still exists, it's just that my local reference to it has been nulled.
        //Which beggers belief, hence the sanity tests here.
        System.out.println("Doing sanity test");
        if (registerDialog == null)
            System.out.println("it's NULL");
        else
            System.out.println("valid");
        registerDialog.setVisible(false);
    }
}

//This simple little dialog is used to set the IP address and port number.
class optionsDialog extends JDialog
{
    platform platform;
    String IP;
    int port;
    
    InetSocketAddress result;
    
    optionsDialog(Window frame,platform platform,String IPref,int portref)
    {
        super(frame,"Options", Dialog.ModalityType.DOCUMENT_MODAL);
        this.platform = platform;
        this.IP = IPref;
        this.port = portref;
        
        JLabel IPLabel = new JLabel("IP address: ",SwingConstants.LEFT);
        IPLabel.setPreferredSize(new Dimension(100,20));
        JLabel portLabel= new JLabel("Port number: ",SwingConstants.LEFT);
        portLabel.setPreferredSize(new Dimension(100,20));
        
        JTextField IPField = new JTextField();
        IPField.setPreferredSize(new Dimension(100,20));
        IPField.setText(IP);
        
        JTextField portField = new JTextField();
        portField.setPreferredSize(new Dimension(100,20));
        portField.setText(""+port);
        
        JButton ok = new JButton("Ok");
        JButton cancel = new JButton("Cancel");
        
        //setting actions.
        ok.addActionListener(new ActionListener()//on a click on the ok button, should result 
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                IP = IPField.getText();
                IPField.setText("");
                
                port = Integer.parseInt(portField.getText() );
                portField.setText("");
                
                result = new InetSocketAddress(IP,port);
                setVisible(false);
                dispose();
            }            
        });
        cancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
            }
        });

        //keyboard shortcuts
        IPField.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    portField.requestFocusInWindow();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    cancel.doClick();
            }
        });
        portField.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    ok.doClick();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    cancel.doClick();
            }
        });
        ok.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    ok.doClick();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    cancel.doClick();
            }
        });
        cancel.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    cancel.doClick();
                else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    cancel.doClick();
            }
        });
        
        //layouting
        JPanel IPPane = new JPanel();
        IPPane.add(IPLabel);
        IPPane.add(IPField);
        
        JPanel PortPane = new JPanel();
        PortPane.add(portLabel);
        PortPane.add(portField);

        JPanel ButtonPane = new JPanel();
        ButtonPane.add(ok);
        ButtonPane.add(cancel);
        
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.Y_AXIS) );
        contentPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        contentPane.add(IPPane);
        contentPane.add(PortPane);
        contentPane.add(ButtonPane);
        
        //final configurations
        setLayout(new BorderLayout() );
        setContentPane(contentPane);

        setLocationRelativeTo(frame);
        pack();
        setResizable(false);
        
        IPField.requestFocusInWindow();
    }
    
    public InetSocketAddress showDialog()
    {
        setVisible(true);
        return result;
    }
}

class registerDialogGUI extends JDialog implements ActionListener
{
    platform platform;
    String username;
    String password;
    String confirmPS;
    
    
    registerDialogGUI(Window frame,platform platformref)
    {
        super(frame,"Register new profile.",Dialog.ModalityType.DOCUMENT_MODAL );
        platform = platformref;
        
        //various fields, buttons and so forth.        
        JLabel usernameLabel = new JLabel("Username: ",SwingConstants.RIGHT );
        JLabel passwordLabel = new JLabel("Password: ",SwingConstants.RIGHT );
        JLabel confirmPSLabel = new JLabel("Confirm Password: ",SwingConstants.RIGHT );
        
        JTextField usernameField = new JTextField();
        JPasswordField passwordField = new JPasswordField();
        JPasswordField confirmPSField = new JPasswordField();
        
        JButton okButton = new JButton("Register");
        okButton.addActionListener(new ActionListener()
        {
            /*If the user pressed register, i.e user confirms he wants to create a new profile and forward that request to the server,
            then we make a check to see if the fields entered are valid, if not it fails.
            The client only verifies that the password and confirm password fields are identical, 
            the server verifies whether or not the username is valid or not, ie already in use.
                */
            @Override
            public void actionPerformed(ActionEvent e)
            {
                username = usernameField.getText();
                password = String.valueOf(passwordField.getPassword() );
                confirmPS = String.valueOf(confirmPSField.getPassword() );
                
                if ( confirmPS.equals(password) )//cool, schedule a task on the networker thread via the appropriate runner.
                {
                    DoProfileRunner runner = new DoProfileRunner(platform.getNetworker(),username,password,REGISTERATTEMPT);
                    platform.getExecutor().execute(runner);
                }
                else//if the user ended wrong data, ie mismatching password fields, then the user needs to be told without the dialog ending.
                {
                    JOptionPane.showMessageDialog(frame,"You failed to confirm your password, please try again",
                        "Password confirmation unsuccessful.", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        
        usernameField.setSize(new Dimension(20,20));
        passwordField.setSize(new Dimension(20,20));
        confirmPSField.setSize(new Dimension(20,20));
        
        //Layouts
        JPanel usernamePanel = new JPanel();
        JPanel passwordPanel = new JPanel();
        JPanel confirmPSPanel = new JPanel();
        
        usernamePanel.setLayout(new GridLayout(1,2));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(usernameField);
        
        passwordPanel.setLayout(new GridLayout(1,2));
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);
        
        confirmPSPanel.setLayout(new GridLayout(1,2));
        confirmPSPanel.add(confirmPSLabel);
        confirmPSPanel.add(confirmPSField);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        JPanel outer = new JPanel();
        outer.setLayout(new BoxLayout(outer,BoxLayout.Y_AXIS) );
        outer.add(usernamePanel);
        outer.add(Box.createVerticalStrut(5));
        outer.add(passwordPanel);
        outer.add(Box.createVerticalStrut(5));
        outer.add(confirmPSPanel);
        outer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        setLayout(new BorderLayout() );
        add(outer,BorderLayout.NORTH);
        add(buttonPanel);
        
        setResizable(false);
        pack();
        setLocationRelativeTo(frame);
        setModal(true);
        setVisible(true);
    }

    //Called when the cancel button is pressed
    @Override
    public void actionPerformed(ActionEvent e)
    {
        dispose();
    }    
    
}
