package gholdem;

/*
This container class is the base class used to hold two fully generic variables: the platform and opcode. 
The specialized subclasses will hold any remaining data. Using a container like this enables us to pass specific 
datasets around without having to specify the arguments manually all the time, and for every link in the chain. 
I realized removing the many genericReceivers that were present in the original always meant tiresome verbosity in way or another. 
A lack of elegance was the result anyway, the problem of verbosity was replaced by just a different kind of verbosity, which sort of defeats the purpose
to begin with.
The solution was indirection: Now, whenever the networker receives a packet, I merely pack those specific data into a equally specific subtyped container
and then pass it to the one and GUISchedulingRunner constructor. The packet, which I call a parcel, is then stored in the runner and handed over to the platform
upon being executed. The platform in turn passes it on to the right GUI with its respective receiver without having to respecify the argument list, which 
elminates the chance of human error(which I figured is bound to happen).

We now only have one receiver per GUI class, one invocation method, and the only time the parcal has to be typechecked is when they reach their endpoints.

That aside a few notes: 
The dataContainer itself can be used with parcels that consists of the opcode alone.
the remaining classes all extend dataContainer because all messages have opcodes. In addition dataContainer is the type the runner,platform and GUI
classes hold a reference to. One polymorphic variable for all. 
*/
public class dataContainer
{
    protected int opcode;
    
    public dataContainer( int opcode)
    {
        this.opcode = opcode;
    }
    
    public int getOpcode() { return opcode; }
}

class dataContainer_one extends dataContainer
{
    int value1;
    public dataContainer_one(int opcode, int value1)
    {
        super(opcode);
        this.value1 = value1;
    }
    
    public int getValue1() { return value1; }
    
}

class dataContainer_two extends dataContainer
{
    int value1;
    int value2;
    
    public dataContainer_two(int opcode, int value1,int value2)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    
}

class dataContainer_twoSpecial_intArray extends dataContainer
{
    int value1;
    int value2;
    int[] data;
    
    public dataContainer_twoSpecial_intArray(int opcode, int value1,int value2,int[] data)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
        this.data = data;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    public int[] getData() { return data; }
}

class dataContainer_three extends dataContainer
{
    int value1;
    int value2;
    int value3;
    
    public dataContainer_three(int opcode, int value1,int value2,int value3)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    public int getValue3() { return value3; }   
}

class dataContainer_threeSpecial_String extends dataContainer
{
    int value1;
    int value2;
    int value3;
    String data;
    
    public dataContainer_threeSpecial_String(int opcode, int value1,int value2,int value3,String data)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.data = data;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    public int getValue3() { return value3; }   
    public String getData() { return data; }
}

class dataContainer_four_Special_StringArray extends dataContainer
{
    int value1;
    int value2;
    int value3;
    int value4;
    String data;
    
    public dataContainer_four_Special_StringArray(int opcode, int value1,int value2,int value3,int value4,String data)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.data = data;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    public int getValue3() { return value3; }
    public int getValue4() { return value4; }
    public String getData() { return data; }
    
}

class dataContainer_five extends dataContainer
{
    int value1;
    int value2;
    int value3;
    int value4;
    int value5;
    
    public dataContainer_five(int opcode, int value1,int value2,int value3,int value4, int value5)
    {
        super(opcode);
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.value5 = value5;
    }
    
    public int getValue1() { return value1; }
    public int getValue2() { return value2; }
    public int getValue3() { return value3; }
    public int getValue4() { return value4; }
    public int getValue5() { return value5; }
}

class dataContainerSpecial_intArray_string_intArray extends dataContainer
{
    int[] intArray1;
    String string;
    int[] intArray2;

    public dataContainerSpecial_intArray_string_intArray(int opcode,int[] intArray1,String string, int[] intArray2)
    {
        super(opcode);
        this.intArray1 = intArray1;
        this.string = string;
        this.intArray2 = intArray2;
    }
    public int[] getArray1() { return intArray1; }
    public String getString() { return string; }
    public int[] getArray2() { return intArray2; }   
}