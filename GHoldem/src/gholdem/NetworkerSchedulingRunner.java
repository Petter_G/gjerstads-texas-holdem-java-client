package gholdem;

import static gholdem.protocolEnums.SERVERCOMMANDS.*;

/*
A "runner" refers to classes that implements the Runnable interface.
Any use of the java swing framework will involve the automatic creation of the "event dispatch thread", or EDT for short.
Similarily the java.util.concurrent package defines the executors, neat implementations of various concurrency related ideas. 
Both the swing framework and the concurrency framework require you to implement your own subclass of the Runnable interface.
These subtypes can then include data you want to share, as well as what you actually want the task to do in the form of overriding the run() method.

In Qt I could rely on the signals and slots mechanism, here I will have to do it myself.
If you've studied the original implementation, you will recognice that the names of these runners are the same as the signals used 
to communicate with the networker there.
*/

//NetworkerSchedulingRunner(s) enable the GUI classes to schedule jobs on the worker. for the most part that will mean sending messages to the server.
abstract class NetworkerSchedulingRunner implements Runnable
{
    protected Networker worker;
        
    public NetworkerSchedulingRunner(Networker workerref)
    {
        worker = workerref;
    }

    @Override
    abstract public void run();
}

class DisconnectFromServerRunner extends NetworkerSchedulingRunner
{
    public DisconnectFromServerRunner(Networker workerref)
    {
        super(workerref);
    }

    @Override
    public void run()
    {
        worker.disconnectFromServer();
    }
    
}

//schedules doProfile protocol attempt with the given arguments.
class DoProfileRunner extends NetworkerSchedulingRunner
{
    private boolean login;
    private String username;
    private String password;
    
    public DoProfileRunner(Networker workerref, String usernameref,String passwordref, boolean logintype)
    {
        super(workerref);
        username = usernameref;
        password = passwordref;
        login = logintype;
    }
    
    @Override
    public void run()
    {
        worker.doProfile(username, password, login);
    }
}

//schedules the networker to send a chat message.
class ChatMessageToSocketRunner extends NetworkerSchedulingRunner
{
    String message;
    
    public ChatMessageToSocketRunner(Networker workerref,String messageref) 
    { 
        super(workerref);
        message = messageref;
    }
    @Override
    public void run()
    {
        worker.writeMessageToSocket(message);
    }
}

//Schedules the networker to request a namechange on the user.
class WriteDisplayNameChangeRequestToSocketRunner extends NetworkerSchedulingRunner
{
    String name;
    public WriteDisplayNameChangeRequestToSocketRunner(Networker networkerref,String nameref)
    {
        super(networkerref);
        name = nameref;
    }
        
    @Override
    public void run()
    {
        worker.writeDisplayNameChangeRequestToSocket(name);
    } 
}

/*These generic commands represents various requests that all have in common they only send one,two or three ints to the server.
This meant that these commands could be generalized into three functions, this ofcourse meant that there had to be three command 
receivers in the networker.

This time around what used to be signals(functions basically) will all be placed in one big runner, where a switch will take care of the 
decision making. Originally there were three overloaded functions, and while the approach worked, I wasn't terribly fond of having three switches for what was
essentially the same decision making process.*/
class GenericCommandRunner extends NetworkerSchedulingRunner
{
    int opcode;
    int value;
    int value2;
    int value3;
    
    String stringValue1;
    String stringValue2;
    
    public GenericCommandRunner(Networker workerref,int opcoderef)
    {
        super(workerref);
        opcode = opcoderef;
    }
    
    public GenericCommandRunner(Networker workerref,int opcoderef,int valueref)
    {
        super(workerref);
        opcode = opcoderef;
        value = valueref;
    }
    
    public GenericCommandRunner(Networker workerref,int opcoderef,int valueref,int value2ref)
    {
        super(workerref);
        opcode = opcoderef;
        value = valueref;
        value2 = value2ref;
    }
    
    public GenericCommandRunner(Networker workerref,int opcoderef,int valueref,int value2ref,int value3ref)
    {
        super(workerref);
        opcode = opcoderef;
        value = valueref;
        value2 = value2ref;
        value3 = value3ref;
    }
    
    public GenericCommandRunner(Networker workerref,int opcoderef,int valueref,int value2ref,int value3ref,String str1ref)
    {
        super(workerref);
        opcode = opcoderef;
        value = valueref;
        value2 = value2ref;
        value3 = value3ref;
        stringValue1 = str1ref;
    }
    
    public GenericCommandRunner(Networker workerref,int opcoderef,String value1,String value2)
    {
        super(workerref);
        opcode = opcoderef;
        stringValue1 = value1;
        stringValue2 = value2;
    }
    
    @Override
    public void run()
    {
        switch (opcode)
        {
            case CALL:
            case ALLIN:
            case FOLD:
            case LEAVEROOM:
                worker.genericCommandReceiver(opcode,0,0,0,null,null);//those commands that consists of a opcode only
                break;
            case RAISE:
            case REFILL:
            case TRANSFER:
                worker.genericCommandReceiver(opcode, value,0,0,null,null);//opcode + one argument
                break;
            case JOIN:
                worker.genericCommandReceiver(opcode, value, value2,value3,stringValue1,null);
                break;
            case CREATE:
                worker.genericCommandReceiver(opcode, 0,0,0,stringValue1,stringValue2);
                break;
            default:
                System.out.println("Error: opcode(\""+opcode+") unknown." );
                break;
        }
    }
}