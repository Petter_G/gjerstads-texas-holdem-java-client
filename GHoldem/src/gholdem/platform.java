package gholdem;

import static gholdem.protocolEnums.CLIENTENUMS.*;
import static gholdem.protocolEnums.MessageEnums.*;
import static gholdem.protocolEnums.DIV.*;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class platform
{
    /* The platform can be seen as the GUI control room for this application. The platform is created and run on the EDT. 
    Any updates to any of the GUI components have to go through this class in the form of the processInvocation method.
    I might add that the advantage of this design is that the networker doesn't need to know about the existance of the GUIs.
    It's a case of decoupling basically.
    
    The original design forced the networker to invoke a "hereIam" function in order to tell the GUIs that it existed, less than ideal.
    */
    private final ExecutorService worker;
    private Networker networker;
    private final loginGUI login;
    private final lobbyGUI lobby;
    private final holdemGUI holdem;
    private final JFrame frame;
    
    private final GUIMetaData loginPoint;
    private final GUIMetaData lobbyPoint;
    private final GUIMetaData holdemPoint;
    
    public platform()
    {
        //First of all we need to load the servers IP and port, if they exist that is. Either way IP and port targets will be set to something.
        //if invocation returns false, we'll show a message dialog making the user aware that default values are in use.
        if ( !ConfigHandler.loadConfigurationFromFile() )//false = config file didn't exist and had to be created with default values.
            JOptionPane.showMessageDialog(null, "Attention: No configuration file was found. \n"
                + "Default values ( localhost::3500) have been loaded.\n"
                + "Please access the options menu to configure manually.","No configuration file detected.",JOptionPane.WARNING_MESSAGE);
        
        //create the GUIs straight away, we only need switch between them as the user navigates the program.
        login = new loginGUI(this);
        lobby = new lobbyGUI(this);
        holdem = new holdemGUI(this);
        
        //Prestoring the panel locations in the form of Point objects. These come in handy later on when we need to reposition the frame depending
        //on which panel is loaded at the time. These positions are used once.
        
        loginPoint = new GUIMetaData(this.getCenteredOffsets(login) );
        lobbyPoint = new GUIMetaData(this.getCenteredOffsets(lobby) );
        holdemPoint = new GUIMetaData(this.getCenteredOffsets(holdem) );
        
        frame = new JFrame();
        frame.setSize(600,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        frame.setVisible(true);

        //This executor exists as a means the platform(by extension the EDT) can offload heavy non-GUI work defined in the networker, like sending messages to the server.
        worker = Executors.newSingleThreadExecutor();
        
        //We need a Networker object in order to facilitate communication and processing of packages coming over a tcp connection with the server.
        worker.execute(() ->
        {
            networker = new Networker(this);
        });
        //set loginGUI as frame content
        switchToLoginPanel();
    }
    
    //Processinvocations represent the means the networker class can schedule updates to the GUI without messing with those objects directly. 
    //This is done by scheduling tasks on the EDT, and these tasks will eventually invoke these methods.
    //Previously there were many invocations, some generic, some specific, I've decided that the best approach would either be one method per opcode, 
    //or one method for all.
    //The previous system was basically a mix between those two, and largely represented the worst of both worlds.    
    public void processInvocation(dataContainer container)
    {
        switch ( container.getOpcode() )
        {
            case PLAYERSTATECHANGEDNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case PLAYERLEFTLOCALROOMNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case PLAYERJOINEDROOMNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case JOINFAILURENOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case JOINSUCCESSNOTIFY:
            {
                this.switchToHoldemGUI();
                break;
            }
            case NEWPLAYERNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case NEWGAMENOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case AUTHENTICATIONSUCCESSNOTIFY:
            {
                this.switchToLobbyGUI();
                break;
            }
            case AUTHENTICATIONFAILURENOTIFY:
            {
                JOptionPane.showMessageDialog(frame, "Your login attempt was rejected. Please check your spelling and try again. "
                    ,"Authentication failure.",JOptionPane.INFORMATION_MESSAGE);
                break;
            }            
            case INVALIDPROFILECREATION:
            {
                //something went wrong, give tips as to what.
                JOptionPane.showMessageDialog(null, "Your requested username was rejected. This is either because the username is already in use, "
                    + "or the characters are illegal. \n"
                    + "Please make sure the username is between "+ MIN_NAMELENGTH + " and " + MAX_NAMELENGTH + " characters long and "
                    + "containing alphanumeric characters only."
                    , "Profile registration failed.",
                        JOptionPane.WARNING_MESSAGE);
                break;
            }
            case VALIDPROFILECREATION:
            {
                //everything is good, close down the register dialog.
                JOptionPane.showMessageDialog(login, "Profile creation succeeded, please proceed to log in with your new profile.", 
                    "Profile registration successful.", JOptionPane.INFORMATION_MESSAGE);
                login.closeRegisterDialog();
                break;
            }
            case SERVERSHUTDOWNNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case REPAINT:
            {
                holdem.genericReceiver(container);
                break;
            }
            case PLAYERSNOTREADY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case TABLESLOTNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case INVALIDCOMMAND:
            {
                Container panel = frame.getContentPane();
                if ( panel instanceof holdemGUI )
                    holdem.genericReceiver(container);
                else if ( panel instanceof lobbyGUI )
                    lobby.genericReceiver(container);
                break;
            }
            case VALIDCOMMAND:
            {
                holdem.genericReceiver(container);
                break;
            }
            case NAMECHANGEDNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case TABLESLOTCHANGEDNAMENOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case NAMECHANGEINVALIDNOTIFY:
            {
                //if the user is ins
                lobby.genericReceiver(container);
                holdem.genericReceiver(container);
                break;
            }
            case HANDUPDATE:
            {
                holdem.genericReceiver(container);
                break;
            }
            case DELETEREP:
            {
                holdem.genericReceiver(container);
                break;
            }
            case BETUPDATE:
            {
                holdem.genericReceiver(container);
                break;
            }
            case BALANCEUPDATE:
            {
                holdem.genericReceiver(container);
                break;
            }
            case ACTIVEUPDATE:
            {
                holdem.genericReceiver(container);
                break;
            }
            case ALLINUPDATE:
            {
                holdem.genericReceiver(container);
                break;
            }
            case NAMETOLONGNOTIFY:
            {
                Container panel = frame.getContentPane();
                if ( panel instanceof holdemGUI )
                    holdem.genericReceiver(container);
                else if ( panel instanceof lobbyGUI )
                    lobby.genericReceiver(container);
                break;
            }
            case TABLERESET:
            {
                holdem.genericReceiver(container);
                break;
            }
            case COMMUNITYCARDADDED:
            {
                holdem.genericReceiver(container);
                break;
            }
            case POTNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case CURRENTPLAYERNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case CURRENTBUTTONNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case MINIMUMRAISEFAILURENOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case PAUPERFAILURENOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case CHECKINVALIDNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case NOTYOURTURNNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case GAMEHALTEDNOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case GAMEPAUSEDNOTIFY:
            {
                if ( frame.getContentPane() instanceof lobbyGUI )
                    lobby.genericReceiver(container);
                else
                    holdem.genericReceiver(container);
                break;
            }
            case WINNERINFONOTIFY:
            {
                holdem.genericReceiver(container);
                break;
            }
            case MAXROOMSCREATEDNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case INVALIDROOMNAMELENGTHNOTIFY: 
            {
                lobby.genericReceiver(container);
                break;
            }
            case INVALIDROOMPSWLENGTHNOTIFY: 
            {
                lobby.genericReceiver(container);
                break;
            }
            case INVALIDROOMPSWNOTIFY: 
            {
                lobby.genericReceiver(container);
                break;
            }
            case PSWPROTECTEDNOTIFY: 
            {
                lobby.genericReceiver(container);
                break;
            }
            case INVALIDROOMNAMEEXISTSNOTIFY:
            {
                lobby.genericReceiver(container);
                break;
            }
            case CHAT:
            {
                //only opcode where we need to analyze the content before we pass it on.
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                int tableslot = ref.getValue1();
                if ( tableslot == -1 )
                    lobby.genericReceiver(container);
                else
                    holdem.genericReceiver(container);
                break;
            }
            default:
            {
                throw new UnsupportedOperationException("Error: Support for opcode: "+container.getOpcode()+" has not been implemented, please investigate.");
            }
        }
    }
    
    //-------------------------End of invocations---------------------------------
    
    public void resetGUI()
    {
        lobby.clearGUI();
        holdem.clearGUI();
        
        this.switchToLoginPanel();
    }
    
    public void switchToLoginPanel()
    {
        //These methods are self descriptive, the resizing is done due to the fact that each panel is of different size and that the layoutmanager may
        //need permission to change the framesize.
        frame.setLocation(loginPoint.getLocation().x,loginPoint.getLocation().y);
        frame.setContentPane(login);
        frame.pack();
        
        login.takeFocus();
    }
    
    public void switchToLobbyGUI()
    {
        frame.setLocation(lobbyPoint.getLocation().x,lobbyPoint.getLocation().y);
        frame.setContentPane(lobby);
        frame.pack();
        
        lobby.takeFocus();
    }
    
    public void switchToHoldemGUI()
    {
        frame.setLocation(holdemPoint.getLocation().x,holdemPoint.getLocation().y);
        frame.setContentPane(holdem);
        frame.pack();
        
        holdem.takeFocus();
    }
    
    //this little function gives us the location on the screen we need if we want the components to be truly centered
    //Just using frame.setLocation(null) will center components corners on screen, not the center of each component, in other words we need offsets.
    private Point getCenteredOffsets(JPanel panel)
    {
        Dimension panelRectangle = panel.getPreferredSize();
        Dimension panelOffsets = new Dimension( panelRectangle.width /2, panelRectangle.height /2 );
        
        Point screenCenter = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        
        return new Point(screenCenter.x - panelOffsets.width,screenCenter.y - panelOffsets.height);
    }
    
    private class GUIMetaData
    {
        Point initialLocation;
        boolean isMaximized;
        
        public GUIMetaData(Point ref)
        {
            initialLocation = ref;
            isMaximized = false;
        }
        public void setIsMaximized(boolean condition) { isMaximized = condition; }
        
        public boolean getIsMaximized() { return isMaximized; }
        
        public Point getLocation() { return initialLocation; }
                
    }
    
    public Networker getNetworker() { return networker; }
    
    public ExecutorService getExecutor() { return worker; }
 
    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                platform platform = new platform( );
                System.out.println("starting thread id: "+Thread.currentThread() );
            }
        });
    }
}
