package gholdem;

import static gholdem.protocolEnums.CLIENTENUMS.*;
import static gholdem.protocolEnums.SERVERCOMMANDS.*;
import static gholdem.protocolEnums.DIV.*;
import static gholdem.protocolEnums.MessageEnums.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;

public class holdemGUI extends JPanel
{

    List<WinnerData> winnerInfo;
    platform platform;

    //various elements used in the GUI
    tableView tableView;

    AutoDatingJTextArea chatArea;
    AutoDatingJTextArea consoleArea;

    JTextField inputField;


    public holdemGUI(platform parentref)
    {
        platform = parentref;
        
        //Initialize the container we'll use later when the server supplies information about winners about the current play.
        winnerInfo = new ArrayList<>();
        
        //logo
        //Originally the logo was shown in the tableview itself.
        //In light of how that doesn't look right, and how difficult it would be to implement properly, 
        //I've decided to simply give the logo its container and place it at the top instead.
        JLabel logo = new JLabel();
        String imageURL = RESOURCEBASE + "transparantLogo.png";
        ImageIcon icon = new ImageIcon(getClass().getClassLoader().getResource(imageURL));
        logo.setIcon(icon);
        logo.setHorizontalAlignment(JLabel.CENTER);
        logo.setVerticalAlignment(JLabel.CENTER);
        
        //menus
        JMenuBar bar = new JMenuBar();
        JMenu game = new JMenu("Game");
        game.setMnemonic(KeyEvent.VK_G);
        JMenu user = new JMenu("User");
        user.setMnemonic(KeyEvent.VK_U);

        //menu items, organized in two camps due to there being two menus
        JMenuItem refillCheat = new JMenuItem("Refill cheat");
        refillCheat.setMnemonic(KeyEvent.VK_R);
        JMenuItem escrowTransfer = new JMenuItem("Escrow transfer");
        escrowTransfer.setMnemonic(KeyEvent.VK_E);

        JMenuItem leaveRoom = new JMenuItem("Leave room");
        leaveRoom.setMnemonic(KeyEvent.VK_R);
        JMenuItem logout = new JMenuItem("Logout");
        logout.setMnemonic(KeyEvent.VK_L);
        JMenuItem changeName = new JMenuItem("Change name");
        changeName.setMnemonic(KeyEvent.VK_C);

        //Menu Items need action listeners in order to actually do something
        refillCheat.addActionListener((ActionEvent e) -> 
        {
            onRefillCheat();
        });
        escrowTransfer.addActionListener((ActionEvent e) -> 
        {
            onEscrowTransfer();
        });

        leaveRoom.addActionListener((ActionEvent e) -> 
        {
            onLeaveRoom();
        });
        logout.addActionListener((ActionEvent e) -> 
        {
            onLogout();
        });
        changeName.addActionListener((ActionEvent e) -> 
        {
            onChangeName();
        });

        //add items to their respective menus
        game.add(refillCheat);
        game.add(escrowTransfer);

        user.add(leaveRoom);
        user.add(logout);
        user.add(changeName);

        bar.add(game);
        bar.add(user);

        //table view. originally I just used a QWidget, which in Swing is the JPanel
        tableView = new tableView();

        //Buttons, ie call,raise etc.
        //Attention, in order to get the damn buttons to be the same size, I had to cheat a little, hence the spaces in call and fold.
        JButton callButton = new JButton ("Call ");
        JButton allinButton = new JButton("Allin");
        JButton raiseButton = new JButton("Raise");
        JButton foldButton = new JButton ("Fold ");
        
        //hints, ie "tooltips"
        callButton.setToolTipText("Match the bid, if any, and end your play in the current turn.");
        allinButton.setToolTipText("Raise with every penny you got. \nBe careful though, if you're bluffing, it will cost you.");
        raiseButton.setToolTipText("Raise the current bet. \nMinimum raise is the big blind and, if applicable, whatever you'd need to match the current bid.");
        foldButton.setToolTipText("Fold your hand, forfeiting any bets you've placed.");

        //set Listeners
        callButton.addActionListener((ActionEvent e) -> 
        {
            onCall();
        });
        allinButton.addActionListener((ActionEvent e) -> 
        {
            onAllin();
        });
        raiseButton.addActionListener((ActionEvent e) -> 
        {
            onRaise();
        });
        foldButton.addActionListener((ActionEvent e) -> 
        {
            onFold();
        });
        
        //initalizing these panels straight away, we'll need initilized so we can register keylisteners on buttonPanel.
        JPanel textChannelPanel = new JPanel();
        JPanel inputFieldPanel = new JPanel();
        JPanel buttonPanel = new JPanel();
        
        addKeyListener(new KeyAdapter()  
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                switch (e.getKeyCode() )
                {
                    case KeyEvent.VK_C:
                        onCall();
                        break;
                    case KeyEvent.VK_A:
                        onAllin();
                        break;
                    case KeyEvent.VK_R:
                        onRaise();
                        break;
                    case KeyEvent.VK_F:
                        onFold();
                        break;
                    case KeyEvent.VK_ENTER:
                        inputField.requestFocusInWindow();
                        break;
                    default:
                        System.out.println("button: "+e.getKeyCode() );
                        break;
                }
            }
        });
        setFocusable(true);

        //chat and console displays and input field
        chatArea = new AutoDatingJTextArea();
        chatArea.setEditable(false);

        consoleArea = new AutoDatingJTextArea();
        consoleArea.setEditable(false);
        
        //For some reason I actually have to configure these two options. By default textAreas do not automatically wrap at line endings, that's what
        //setLineWrap fixes, and equally wierd they wrap at the last character instead of last word, as I see it, both options ought to be the default.
        chatArea.setLineWrap(true);
        chatArea.setWrapStyleWord(true);
        consoleArea.setLineWrap(true);
        consoleArea.setWrapStyleWord(true);

        inputField = new JTextField();
        inputField.setToolTipText("Write your message here.");
        inputField.addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER )
                {
                    onInput();
                }
            }  
        });
        
        //Layouting
        
        //the logoWrapper exists solely in order to have something to attach a background color to. 
        //Applying a background color directly to the logo had no effect. Quite frankly I am surprised I had to do these hacks, 
        //It really shouldn't be this messy.
        JPanel logoWrapper = new JPanel();
        logoWrapper.add(logo);
        
        //Equivalent for the bar
        JPanel barWrapper = new JPanel(new BorderLayout());
        barWrapper.add(bar,BorderLayout.CENTER);
        
        //we need a panel to hold the topmost items,like the menu and logo
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.Y_AXIS) );
        topPanel.add(barWrapper);
        topPanel.add(logoWrapper);

        //Moving on to layouting, buttons first
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints BPcons = new GridBagConstraints();
        BPcons.gridy = 0;
        BPcons.insets = new Insets(0, 5, 0, 5);//space between buttons
        BPcons.fill = GridBagConstraints.NONE;

        buttonPanel.add(callButton, BPcons);
        buttonPanel.add(raiseButton, BPcons);
        buttonPanel.add(foldButton, BPcons);
        buttonPanel.add(allinButton, BPcons);

        //The two JTextAreas
        JScrollPane chatAreaScroller = new JScrollPane(chatArea);
        JScrollPane consoleAreaScroller = new JScrollPane(consoleArea);
        
        chatAreaScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        consoleAreaScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        textChannelPanel.setLayout(new GridBagLayout());
        GridBagConstraints textCons = new GridBagConstraints();
        textCons.gridy = 0;
        textCons.gridx = 0;
        textCons.weightx = 90;
        textCons.weighty = 90;
        textCons.fill = GridBagConstraints.BOTH;
        textChannelPanel.add(chatAreaScroller, textCons);
        textCons.gridx = GridBagConstraints.RELATIVE;
        textChannelPanel.add(Box.createRigidArea(new Dimension(5,5)));// for some bizare reason, regular struts caused problems here.
        textChannelPanel.add(consoleAreaScroller, textCons);
        textChannelPanel.setPreferredSize(new Dimension(5, 200));

        //InputField config
        inputFieldPanel.setLayout(new BoxLayout(inputFieldPanel, BoxLayout.X_AXIS));
        inputFieldPanel.add(inputField);

        //Now that the wrappers are made, lets mash them all together.
        setLayout(new GridBagLayout());
        
        GridBagConstraints constraint = new GridBagConstraints();
        GridBagConstraints verticalSpaces = new GridBagConstraints();
        
        verticalSpaces.fill = GridBagConstraints.BOTH;
        verticalSpaces.weighty = 0;
        verticalSpaces.gridy = GridBagConstraints.RELATIVE;
        verticalSpaces.gridx = 0;
        
        constraint.gridy = 1;
        constraint.gridx = 0;
        constraint.fill = GridBagConstraints.BOTH;
        constraint.weighty = 1.00;
        constraint.weightx = 1.00;

        add(topPanel,constraint);
        constraint.gridy = GridBagConstraints.RELATIVE;
        add(tableView, constraint);
        constraint.fill = GridBagConstraints.HORIZONTAL;
        constraint.insets = new Insets(5, 5, 5, 5);//int top, int left, int bottom, int right
        add(buttonPanel, constraint);
        constraint.insets = new Insets(0,0,0,0);
        constraint.fill = GridBagConstraints.BOTH;
        constraint.weighty = 180;
        add(textChannelPanel, constraint);
        constraint.weighty = 0;
        add(Box.createVerticalStrut(5),verticalSpaces);
        constraint.fill = GridBagConstraints.BOTH;
        constraint.weighty = 0;
        add(inputFieldPanel, constraint);
        
        setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5) );

        //finally set a green background befitting a gambling table.
        //might add that the background stuff became messier than anticipated. In Qt, the background color set in a parent component 
        //simply got inherited by the children. Perhaps a result of CSS inspiration, at any rate, no such luck in swing.
        setBackgroundColor_Recursively(tableView, Color.green.darker() );
        setBackgroundColor_Recursively(logoWrapper, Color.green.darker() );
    }
    
    public void takeFocus()
    {
        requestFocusInWindow();
    }

    //Setting a background that actually shines through into subcontents was surprisingly difficult. 
    //We'll have to track down each component and set its background color
    private void setBackgroundColor_Recursively(Container parent,Color color)
    {
        parent.setBackground( color );
        for ( Component comp : parent.getComponents() )
        {
            if (comp instanceof Container)
                setBackgroundColor_Recursively((Container)comp,color);
        }
    }
    

    //Actionlistener callbacks
    private void onRefillCheat()
    {

        Window registerWindow = SwingUtilities.getWindowAncestor(this);
        spinnerQuestionDialog dialog = new spinnerQuestionDialog(registerWindow, this.platform,"please specify how much you want to add to your account:","Escrow refill",
                                            10,REFILL);
    }

    private void onEscrowTransfer()
    {
        Window registerWindow = SwingUtilities.getWindowAncestor(this);
        spinnerQuestionDialog dialog = new spinnerQuestionDialog(registerWindow, this.platform,"Please specify how much you want to transfer from your escrow account","Money transfer",
                                            10,TRANSFER);
    }

    private void onLeaveRoom()
    {
        int result = JOptionPane.showConfirmDialog(null, "Return to the lobby?","Leave room?",JOptionPane.YES_NO_OPTION);
        
        if (result == JOptionPane.YES_OPTION)
        {
            GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(),LEAVEROOM);
            platform.getExecutor().execute(runner);
            
            //can make a static call since we're already in the EDT.
            this.clearGUI();
            platform.switchToLobbyGUI();
        }
        
    }

    private void onLogout()
    {
        int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to logout?","Logout?",JOptionPane.YES_NO_OPTION);
        
        if (result == JOptionPane.YES_OPTION)
        {
            DisconnectFromServerRunner runner = new DisconnectFromServerRunner(platform.getNetworker());
            platform.getExecutor().execute(runner); 
        }
        
    }

    private void onChangeName()
    {
        String newname = JOptionPane.showInputDialog(null, "Please enter your new username:", "Change your nickname?", JOptionPane.QUESTION_MESSAGE);

        if (newname != null && newname.length() > 0)
        {
            WriteDisplayNameChangeRequestToSocketRunner runner = new WriteDisplayNameChangeRequestToSocketRunner(platform.getNetworker(), newname);
            platform.getExecutor().execute(runner);
        }
    }

    private void onCall()
    {    
        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), CALL);
        platform.getExecutor().execute(runner);
    }

    private void onAllin()
    {
        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), ALLIN);
        platform.getExecutor().execute(runner);
    }

    private void onRaise()
    {
        //Annoyingly, I don't get to use builtin dialogs tailored for this particular purpose this time around. No matter, I'll just make my own.
        Window registerWindow = SwingUtilities.getWindowAncestor(this);
        spinnerQuestionDialog dialog = new spinnerQuestionDialog(registerWindow, this.platform,"By how much would you like to raise?","Raise?",20,RAISE);
    }

    private void onFold()
    {
        GenericCommandRunner runner = new GenericCommandRunner(platform.getNetworker(), FOLD);
        platform.getExecutor().execute(runner);
    }

    private void onInput()
    {  
        String input = inputField.getText();
        inputField.setText("");
        if (input.length() != 0)//no point sending a empty message. 
        {
            ChatMessageToSocketRunner runner = new ChatMessageToSocketRunner(platform.getNetworker(), input);
            platform.getExecutor().execute(runner); 
        }
        
        requestFocusInWindow();//Make sure the focus returns to the holdemGUI after user sends message.
    }
    
    //resets the holdemGUI(including subcomponents) to a blank slate, as if just instantiated.
    public void clearGUI()
    {        
        for (handWidget player : tableView.getPlayers() )
        {
            player.setVisible(false);
            player.toggleYOULabel(false);
        }
        
        for( ConvenientJLabel slot : tableView.getCommunitySlots() )
            slot.setVisible(false);
        
        chatArea.setText("");
        consoleArea.setText("");
        
        //clear winnerInfo in case we left during that operation.
        winnerInfo.clear();
    }

    public void genericReceiver(dataContainer container)
    {
        handWidget[] players = tableView.getPlayers();
        
        switch ( container.getOpcode() )
        {
            case TABLESLOTCHANGEDNAMENOTIFY:
            {
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                
                int tableslot = ref.getValue1();
                int silent = ref.getValue2();
                int[] newNameRaw = ref.getData();
                
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < newNameRaw.length; i++)
                {
                    sb.append( (char)newNameRaw[i] );
                }
                String newName = sb.toString();
                
                int index = tableslot - 1; //tableslot = indeks + 1, compensating.
                
                //If silent is false, update the console with a notice.
                if ( silent == 0  )
                {
                    String appendage = "'"+players[index].getName() + "' is now known as '" + newName;
                    consoleArea.append(appendage);
                }
                
                players[index].setPlayername(newName);
                break;
            }
            case HANDUPDATE:
            {
                if ( !(container instanceof dataContainer_five) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_six");
                
                dataContainer_five ref = (dataContainer_five)container;
                
                int index = ref.getValue1() -1;
                players[index].setGFXCard( ref.getValue2(), ref.getValue3(), LEFT);
                players[index].setGFXCard( ref.getValue4(), ref.getValue5(), RIGHT);
                break;
            }
            case BETUPDATE:
            {
                if ( !(container instanceof dataContainer_three) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_three");
                
                dataContainer_three ref = (dataContainer_three)container;
                
                int index = ref.getValue1() - 1;
                int bet = ref.getValue2();
                int totalbet = ref.getValue3();
                players[index].setBet(bet, totalbet);
                break;
            }
            case TABLESLOTNOTIFY:
            {
                if ( !(container instanceof dataContainer_three) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_three");
                
                dataContainer_three ref = (dataContainer_three)container;
                 
                int index = ref.getValue1() - 1;
                boolean trulyNew = (ref.getValue3() == 1 ? true : false);
                String appendage;
                    
                boolean first = (ref.getValue3() == 1 ? true : false);
                if (first)
                {
                    //if first, ie the data is about this particular player and not someone else, display an appropriate message
                    appendage = "Welcome, you have been assigned tableslot " + ref.getValue1();
                    consoleArea.append(appendage);
                    
                    //Notify the corresponding handWidget and tell it to show the YOU label.
                    players[index].toggleYOULabel(true);
                }
                else//someone elses slot
                {
                    //Display a note about the new player's arrival. If trulyNew, show notice. If not, say nothing.
                    if ( trulyNew)
                    {
                        appendage = "New player arrived, tableslot assigned is " + ref.getValue1();
                        consoleArea.append(appendage);
                    }
                    //By default other player's hands are to be hidden, ie backside up.
                    players[index].showBackside();
                    
                }
                players[index].setVisible(true);
                break;
            }
            case CURRENTPLAYERNOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                int newCurrentPlayerIndex = ref.getValue1() -1 ;
                
                //the currentPlayer should be non-transparant, while everyone else should be opaque, ie transparant.
                for (int i = 0; i < players.length; i++)
                    players[i].SetAlpha(true);
                
                players[newCurrentPlayerIndex].SetAlpha(false);
                break;
            }
            case BALANCEUPDATE:
            {
                if ( !(container instanceof dataContainer_two) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_two");
                
                dataContainer_two ref = (dataContainer_two)container;
                
                int index = ref.getValue1() - 1;
                int balance = ref.getValue2();
                players[index].setBalance(balance);
                break;
            }
            case ACTIVEUPDATE:
            {
                if ( !(container instanceof dataContainer_two) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_two");
                
                dataContainer_two ref = (dataContainer_two)container;
                
                int index = ref.getValue1() -1;
                boolean active = (ref.getValue2() == 1 ? true : false);
                
                if ( !active )//aka inactive
                {
                    players[index].markBetFolded();
                    players[index].hideCards();
                }
                break;
            }
            case COMMUNITYCARDADDED:
            {
                if ( !(container instanceof dataContainer_two) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_two");
                
                dataContainer_two ref = (dataContainer_two)container;
                
                tableView.appendCommunity( ref.getValue1(),ref.getValue2() );
                break;
            }
            case PLAYERLEFTLOCALROOMNOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                int index = ref.getValue1() - 1;
                players[index].markNameLeaving();
                players[index].hideCards();
                break;
            }
            case MINIMUMRAISEFAILURENOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                String appendage = "Minimum raise is equal to the bigblind, which is "+ref.getValue1()+". Please try another action.";
                consoleArea.append(appendage);
                break;
            }
            case POTNOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                tableView.setPot( ref.getValue1() );
                break;
            }
            case CURRENTBUTTONNOTIFY:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                int index = ref.getValue1() - 1;
                players[index].toggleDealerButton(true);
                break;
            }
            case ALLINUPDATE:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                int index = ref.getValue1() - 1;
                players[index].markBetAllin();
                break;
            }
            case DELETEREP:
            {
                if ( !(container instanceof dataContainer_one) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_one");
                
                dataContainer_one ref = (dataContainer_one)container;
                
                int index = ref.getValue1() - 1;
                players[index].setVisible(false);
                break;
            }
            case REPAINT:
            {
                //Simple really, convert the winnerInfo list to a human readable format, send it to the console, then wipe winnerInfo cleam.
                consoleArea.append( getFormattedWinners() );
                winnerInfo.clear();
                break;
            }
            case TABLERESET:
            {
                for (int i = 0; i < MAX_PLAYERS; i++)
                {
                    if ( players[i] == null)
                        continue;
                    players[i].setBet(0,0);
                    players[i].showBackside();
                    players[i].toggleDealerButton(false);
                }
                
                //Must also reset community and pot
                for (ConvenientJLabel com : tableView.getCommunitySlots() )
                    com.setVisible(false);
                
                consoleArea.append("New Round Started.");
                break;
            }
            case PLAYERSNOTREADY:
            {
                consoleArea.append("Unable to start: Not all players are ready.");
                break;
            }
            case VALIDCOMMAND:
            {
                consoleArea.append("Valid Command");
                break;
            }
            case INVALIDCOMMAND:
            {
                consoleArea.append("Please try again.");
                break;
            }
            case NAMETOLONGNOTIFY:
            {
                consoleArea.append("Your requested new username was to long. Make sure usernames are no more than "+protocolEnums.DIV.MAX_NAMELENGTH
                + " characters in length.");
                break;
            }
            case PAUPERFAILURENOTIFY:
            {
                consoleArea.append("You cannot afford that action. Please try again.");
                break;
            }
            case CHECKINVALIDNOTIFY:
            {
                consoleArea.append("Checking is not currently possible.");
                break;
            }
            case NOTYOURTURNNOTIFY:
            {
                consoleArea.append("it's not your turn.");
                break;
            }
            case GAMEHALTEDNOTIFY:
            {
                consoleArea.append("Game halted, minimum of two players needed.");
                break;
            }
            case GAMEPAUSEDNOTIFY:
            {
                consoleArea.append("The game is paused, please wait.");
                break;
            }
            case WINNERINFONOTIFY:
            {
                if (!(container instanceof dataContainer_three))
                    throw new IllegalStateException("container supplied is not of type dataContainer_three");

                dataContainer_three ref = (dataContainer_three) container;

                int WinnerTableSlot = ref.getValue1();
                int payout = ref.getValue2();
                int potIndex = ref.getValue3();
                
                this.winnerInfo.add(new WinnerData(WinnerTableSlot,payout,potIndex) );
                break;
            }
            case SERVERSHUTDOWNNOTIFY:
            {
                consoleArea.append("Attention: Server is about to shut down.");
                break;
            }
            case NAMECHANGEINVALIDNOTIFY:
            {
                consoleArea.append("Your attempt to change your username failed.");
                break;
            }
            case CHAT:
            {
                if ( !(container instanceof dataContainer_twoSpecial_intArray) )
                    throw new IllegalStateException("container supplied is not of type dataContainer_twoSpecial_intArray");
                
                dataContainer_twoSpecial_intArray ref = (dataContainer_twoSpecial_intArray)container;
                
                int tableslot = ref.getValue1();
                int fd = ref.getValue2();
                int[] message = ref.getData();
                
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < message.length; i++)
                    sb.append( (char)message[i] );
                
                String displayname = players[tableslot-1].getName();
                
                String formattedMessage = displayname + ": " + sb.toString();
                chatArea.append( formattedMessage );
                
                break;
            }
            default:
            {
                System.out.println("Opcode: " + container.getOpcode() + " Is unknown");
                break;
            }
        }
    }
    
    //creates and returns a nicely formatted human readable string based on the winnerInfo list.
    private String getFormattedWinners()
    {
        HashMap<Integer,Integer> potCounts = new HashMap<>();
        StringBuilder SB = new StringBuilder();
        
        //We'll need to know how many winners there are per pot, for the majority of cases tha answer is one winner, and one pot, but not always.
        for ( WinnerData winner : winnerInfo )
        {
            int potNumber = winner.getPotNumber();
            
            int currentValue = potCounts.getOrDefault(potNumber, 0);
            currentValue++;
            potCounts.put(potNumber, currentValue);
        }
        
        //now that we have the number of winners per pot, we can proceed.
        for (WinnerData winner : winnerInfo )
        {
            int potNumber = winner.getPotNumber();
            int payout = winner.getPayout();
            
            String split;
            if (potCounts.getOrDefault(potNumber, 0) != 1 )
                split = "(split)";
            else 
                split = "";
            
            handWidget[] players = tableView.getPlayers();
            //NOTE: the practice of deriving the index based on tableslot is faulty due to a poor implementation on the server side.
            //chances are that indexes and tableslots will be merged into one at some point, probably by using some datatype not having indexes, like maps.
            String playername = players[ winner.getWinnerTableSlot()-1 ].getName();
            
            SB.append(playername);
            SB.append(" wins ");
            SB.append(payout);
                    
            switch ( potNumber) 
            {
                case 0://IE main pot
                {
                    SB.append(" from the mainpot.");
                    SB.append(split);
                    break;
                }
                case 1: //first sidepot
                {
                    SB.append(" from the 1st sidepot.");
                    SB.append(split);
                    break;
                }
                case 2: //second sidepot
                {
                    SB.append(" from the 2nd sidepot.");
                    SB.append(split);
                    break;
                }
                case 3: //third
                {
                    SB.append(" from the 3rd sidepot.");
                    SB.append(split);
                    break;
                }
                default://nth sidepots
                {
                    SB.append(" from the ");
                    SB.append(potNumber);
                    SB.append("th sidepot.");
                    SB.append(split);
                }  
            }
        }
        return SB.toString();
    }
    
    //this class is self-explainatory. Used in the list at the top.
    public class WinnerData
    {
        final int winnerTableslot,payout,potNumber;
        
        public WinnerData(int winnerTableslot, int payout,int potNumber)
        {
            this.winnerTableslot = winnerTableslot;
            this.payout = payout;
            this.potNumber = potNumber;
        }
        
        public int getWinnerTableSlot()  { return winnerTableslot; }
        public int getPayout() { return payout; }
        public int getPotNumber() { return potNumber; }
    }
}
